/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Oct 11 17:05:37 CEST 2016
 */

package uebung04.ml.aufgabe01;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import uebung02.ml.aufgabe01.BinarySearchTree;

class AVLTreeImpl<K extends Comparable<? super K>, V> extends BinarySearchTree<K, V> {

	/**
	 * After a BST-operation, actionNode shall point to where the balance has to
	 * be checked. -> rebalance() will then be called with actionNode.
	 */
	protected AVLNode actionNode;

	protected class AVLNode extends BinarySearchTree<K, V>.Node {

		private int height;
		private Node parent;

		AVLNode(Entry<K, V> entry) {
			super(entry);
		}

		protected AVLNode setParent(AVLNode parent) {
			AVLNode old = avlNode(this.parent);
			this.parent = parent;
			return old;
		}

		protected AVLNode getParent() {
			return avlNode(parent);
		}

		protected int setHeight(int height) {
			int old = this.height;
			this.height = height;
			return old;
		}

		protected int getHeight() {
			return height;
		}

		@Override
		public AVLNode getLeftChild() {
			return avlNode(super.getLeftChild());
		}

		@Override
		public AVLNode getRightChild() {
			return avlNode(super.getRightChild());
		}

		@Override
		public String toString() {
			String result = String.format("%2d - %-6s : h=%d", getEntry().getKey(), getEntry().getValue(), height);
			if (parent == null) {
				result += " ROOT";
			} else {
				boolean left = (parent.getLeftChild() == this) ? true : false;
				result += (left ? " / " : " \\ ") + "parent(key)=" + parent.getEntry().getKey();
			}
			return result;
		}

	} // End of class AVLNode

	protected AVLNode getRoot() {
		return avlNode(root);
	}

	public V put(K key, V value) {
		Entry<K, V> entry = find(key);
		if (entry != null) {
			// key already exists in the Tree
			return entry.setValue(value);
		} else {
			// key does not exist in the Tree yet
			super.insert(key, value);
			rebalance(actionNode);
			actionNode = null;
			return null;
		}
	}

	public V get(K key) {
		Entry<K, V> entry = super.find(key);
		if (entry != null) {
			return entry.getValue();
		} else {
			return null;
		}
	}

	@Override
	protected Node insert(Node node, Entry<K, V> entry) {
		if (node != null) {
			actionNode = avlNode(node);
		}
		// calling now the BST-insert() which will do the work:
		AVLNode result = avlNode(super.insert(node, entry));
		if (node == null) {
			// In this case: result of super.insert() is the new node!
			result.setParent(actionNode);
		}
		return result;
	}

	/**
	 * The height of the tree.
	 * 
	 * @return The actual height. -1 for an empty tree.
	 */
	@Override
	public int getHeight() {
		return height(avlNode(root));
	}

	/**
	 * Returns the height of this node.
	 * 
	 * @param node
	 * @return The height or -1 if null.
	 */
	protected int height(AVLNode node) {
		return (node != null) ? node.getHeight() : -1;
	}

	/**
	 * Restructures the tree with rotations.
	 * 
	 * @param xPos
	 *            The X-node.
	 * @return The new root-node of this subtree.
	 */
	protected AVLNode restructure(AVLNode xPos) {
		AVLNode yPos = xPos.getParent();
		AVLNode zPos = yPos.getParent();
		AVLNode newSubTreeRoot = null;
		if (yPos == zPos.getLeftChild()) {
			if (xPos == yPos.getLeftChild()) {
				newSubTreeRoot = rotateWithLeftChild(zPos);
			} else {
				newSubTreeRoot = doubleRotateWithLeftChild(zPos);
			}
		} else {
			if (xPos == yPos.getRightChild()) {
				newSubTreeRoot = rotateWithRightChild(zPos);
			} else {
				newSubTreeRoot = doubleRotateWithRightChild(zPos);
			}
		}
		return newSubTreeRoot;
	}

	protected AVLNode tallerChild(AVLNode node) {
		if (height(node.getLeftChild()) >= height(node.getRightChild())) {
			return node.getLeftChild();
		} else {
			return node.getRightChild();
		}
	}

	protected AVLNode rotateWithLeftChild(AVLNode k2) {
		AVLNode k1 = k2.getLeftChild();
		k2.setLeftChild(k1.getRightChild());
		k1.setRightChild(k2);
		// Parents:
		if (k2.getLeftChild() != null) {
			k2.getLeftChild().setParent(k2);
		}
		adjustParents(k2, k1);
		return k1;
	}

	protected AVLNode doubleRotateWithLeftChild(AVLNode k3) {
		k3.setLeftChild(rotateWithRightChild(k3.getLeftChild()));
		return rotateWithLeftChild(k3);
	}

	protected AVLNode rotateWithRightChild(AVLNode k1) {
		AVLNode k2 = k1.getRightChild();
		k1.setRightChild(k2.getLeftChild());
		k2.setLeftChild(k1);
		// Parents:
		if (k1.getRightChild() != null) {
			k1.getRightChild().setParent(k1);
		}
		adjustParents(k1, k2);
		return k2;
	}

	protected AVLNode doubleRotateWithRightChild(AVLNode k3) {
		k3.setRightChild(rotateWithLeftChild(k3.getRightChild()));
		return rotateWithRightChild(k3);
	}

	/**
	 * Assures that the childrens have theirs correct parents. Used after
	 * rotations.
	 * 
	 * @param oldSubtreeRoot
	 *            The old root-node of this subtree.
	 * @param newSubtreeRoot
	 *            The new root-node of this subtree.
	 */
	protected void adjustParents(final AVLNode oldSubtreeRoot, final AVLNode newSubtreeRoot) {
		final AVLNode parentSubtree = oldSubtreeRoot.getParent();
		oldSubtreeRoot.setParent(newSubtreeRoot);
		if (oldSubtreeRoot == root) {
			newSubtreeRoot.setParent(null);
			root = newSubtreeRoot;
		} else {
			newSubtreeRoot.setParent(parentSubtree);
			if (oldSubtreeRoot == parentSubtree.getLeftChild()) {
				parentSubtree.setLeftChild(newSubtreeRoot);
			} else {
				parentSubtree.setRightChild(newSubtreeRoot);
			}
		}
	}

	protected boolean isBalanced(AVLNode node) {
		int bf = height(node.getLeftChild()) - height(node.getRightChild());
		return (-1 <= bf) && (bf <= 1);
	}

	/**
	 * Assures the balance of the tree from 'node' up to the root.
	 * 
	 * @param node
	 *            The node from where to start.
	 */
	protected void rebalance(AVLNode node) {
		while (node != null) {
			setHeight(node);
			if (!isBalanced(node)) {
				AVLNode xPos = tallerChild(tallerChild(node));
				node = restructure(xPos);
				setHeight(node.getLeftChild());
				setHeight(node.getRightChild());
				setHeight(node);
			}
			node = node.getParent();
		}
	}

	/**
	 * Assures the correct height for node.
	 * 
	 * @param node
	 *            The node to assure its height.
	 */
	protected void setHeight(AVLNode node) {
		if (node == null) {
			return;
		}
		AVLNode child = null;
		child = node.getLeftChild();
		int heightLeftChild = -1;
		if (child != null) {
			heightLeftChild = child.getHeight();
		}
		child = node.getRightChild();
		int heightRightChild = -1;
		if (child != null) {
			heightRightChild = child.getHeight();
		}
		node.setHeight(1 + Math.max(heightLeftChild, heightRightChild));
	}

	/**
	 * Factory-Method. Creates a new node.
	 * 
	 * @param entry
	 *            The entry to be inserted in the new node.
	 * @return The new created node.
	 */
	@Override
	protected Node newNode(Entry<K, V> entry) {
		AVLNode avlNode = new AVLNode(entry);
		return avlNode;
	}

	public V remove(K key) {
		Entry<K, V> entry = find(key);
		if (entry == null) {
			return null;
		}
		// calling now the BST-remove(Entry) which will do the work:
		super.remove(entry);
		if (actionNode != null) {
			assureParentForChild(actionNode, actionNode.getLeftChild());
			assureParentForChild(actionNode, actionNode.getRightChild());
			rebalance(actionNode);
			actionNode = null;
		}
		return entry.getValue();
	}

	@Override
	protected RemoveResult remove(final Node node, final Entry<K, V> entry) {
		if (node.getEntry() == entry) {
			actionNode = avlNode(node).getParent();
		}
		// calling now the BST-remove(Node, Entry) which will do the work:
		return super.remove(node, entry);
	};

	@Override
	protected Node getParentNext(Node p) {
		actionNode = avlNode(super.getParentNext(p));
		return actionNode;
	}

	protected void assureParentForChild(AVLNode parent, AVLNode child) {
		if (child != null) {
			child.setParent(parent);
		}
	}

	/**
	 * Generates an inorder-node-list.
	 * 
	 * @param nodeList
	 *            The node-list to fill in inorder.
	 * @param node
	 *            The node to start from.
	 */
	protected void inorder(Collection<AVLNode> nodeList, AVLNode node) {
		if (node == null)
			return;
		inorder(nodeList, node.getLeftChild());
		nodeList.add(node);
		inorder(nodeList, node.getRightChild());
	}

	@SuppressWarnings("unchecked")
	protected AVLNode avlNode(Node node) {
		return (AVLNode) node;
	}

	public void print() {
		List<AVLNode> nodeList = new LinkedList<>();
		inorder(nodeList, avlNode(root));
		nodeList.stream().forEach(node -> {
			System.out.println(node + "  ");
		});
	}

}
