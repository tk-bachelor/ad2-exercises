/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 14 14:44:02 CET 2016
 */

package uebung08.ml.aufgabe02;

import java.util.LinkedList;
import java.util.List;

class TrieNode<V> {

  private String keySubstr;
  private LinkedList<V> values;
  private LinkedList<TrieNode<V>> childs;

  public TrieNode() {
    this.keySubstr = "";
    this.values = new LinkedList<V>();
    this.childs = new LinkedList<TrieNode<V>>();
  }

  public String getKeySubstr() {
    return keySubstr;
  }

  public void setKeySubstr(String keySubstr) {
    this.keySubstr = keySubstr;
  }
  
  public List<TrieNode<V>> getChilds() {
    return childs;
  }

  public List<V> getValues() {
    return values;
  }

}
 
 
 
