/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 14 14:44:02 CET 2016
 */

package uebung08.ml.aufgabe02;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TrieMultimapJUnitTest {

  TrieMultimap<String> trie;

  @Before
  public void setUp() {
    trie = new TrieMultimap<String>();
  }

  @Test
  public void test01Size() {
    assertEquals(0, trie.size());
    trie.insert("key_1", "value_1");
    assertEquals(1, trie.size());
    trie.insert("key_2", "value_2");
    assertEquals(2, trie.size());
  }

  @Test
  public void test02Find() {
    assertNull(trie.find("key_1"));
    trie.insert("key_1", "value_1");
    assertEquals("value_1", trie.find("key_1"));
    trie.insert("key_11", "value_11");
    assertEquals("value_11", trie.find("key_11"));
  }

  @Test
  public void test03FindAll() {
    Iterator<String> it = trie.findAll("key_1");
    assertFalse(it.hasNext());
    trie.insert("key_1", "value_1a");
    trie.insert("key_1", "value_1b");
    trie.insert("key_1", "value_1c");
    it = trie.findAll("key_1");
    for (int i = 0; i < 3; i++) {
      assertTrue(it.hasNext());
      String str = it.next();
      assertTrue(str.equals("value_1a") || str.equals("value_1b")
          || str.equals("value_1c"));
    }
    assertFalse(it.hasNext());
  }

  @Test
  public void test04Remove() {
    trie.insert("key_1", "value_1");
    trie.remove("xxxx");
    assertEquals(1, trie.size());
    trie.remove("key_1");
    assertEquals(0, trie.size());
  }

  @Test
  public void test05StressTest() {

    final int NUMBER_OF_TESTS = 200;
    final int NUMBER_OF_KEYS = 20;
    final int MAX_KEY_LEN = 5;
    final int MAX_NUMBER_OF_VALUES_PER_KEY = 5;

    Map<String, List<String>> map = new LinkedHashMap<>();
    // filling
    for (int testNr = 0; testNr < NUMBER_OF_TESTS; testNr++) {
      for (int keyNr = 0; keyNr < NUMBER_OF_KEYS; keyNr++) {
        String key = "";
        int keyLen = random(1, MAX_KEY_LEN);
        for (int i = 0; i < keyLen; i++) {
          key += (char) random('a', 'd');
        }
        final int NUMBER_OF_VALUES = random(1, MAX_NUMBER_OF_VALUES_PER_KEY);
        for (int valueNr = 0; valueNr < NUMBER_OF_VALUES; valueNr++) {
          String value = "value_" + valueNr;
          trie.insert(key, value);
          List<String> valueList = map.get(key);
          if (valueList != null) {
            valueList.add(value);
          } else {
            valueList = new LinkedList<String>();
            valueList.add(value);
          }
          map.put(key, valueList);
        }
      }
      // comparing
      for (String key : map.keySet()) {
        List<String> mapValues = map.get(key);
        Collections.sort(mapValues);
        List<String> trieValues = new LinkedList<>();
        Iterator<String> it = trie.findAll(key);
        while (it.hasNext()) {
          trieValues.add(it.next());
        }
        Collections.sort(trieValues);
        if (!mapValues.equals(trieValues)) {
          System.err.println("Unexpected values : ");
          System.err.println("Key : " + key);
          System.err.println("Expected values : ");
          System.err.println(mapValues);
          System.err.println("Values in trie  : ");
          System.err.println(trieValues);
          fail("Unexpected result!");
        }
      }
    }
  }

  /**
   * Returns a random-number in the range from..to.
   * 
   * @param from
   *          The Lower-Bound (inclusive).
   * @param to
   *          The Upper-Bound (inclusive).
   * @return The generated random-number.
   */
  private int random(int from, int to) {
    return from + (int) (Math.random() * (to - from + 1));
  }

}
 
 
 
