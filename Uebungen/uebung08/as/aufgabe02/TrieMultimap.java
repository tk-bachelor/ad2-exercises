/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov  7 11:33:32 CET 2016
 */

package uebung08.as.aufgabe02;

import java.util.ArrayList;
import java.util.Iterator;

public class TrieMultimap<V> implements Multimap<String, V> {

	private TrieNode<V> root;

	public TrieMultimap() {
		this.root = new TrieNode<V>();
	}

	/**
	 * Inserts a key/value pair into the multimap.
	 */
	public void insert(String key, V value) {
		insert(root, key, 0, value);
	}

	private void insert(TrieNode<V> currentNode, String key, int keyIndex, V value) {
		if(key.length() == keyIndex){
			// found the node
			currentNode.getValues().add(value);
			return;
		}

		for (TrieNode<V> child : currentNode.getChilds()) {
			if(child.getKeySubstr().charAt(0) == key.charAt(keyIndex)){
				insert(child, key, keyIndex+1, value);
				return;
			}
		}
		
		// there are no node found
		TrieNode<V> newNode = new TrieNode<>();
		currentNode.getChilds().add(newNode);
		
		newNode.setKeySubstr(key.substring(keyIndex, keyIndex+1));
		insert(newNode, key, keyIndex+1, value);
	}
	
	private TrieNode<V> findNode(TrieNode<V> currentNode, String key, int keyIndex) {
		if(key.length() == keyIndex){
			return currentNode;
		}

		for (TrieNode<V> child : currentNode.getChilds()) {
			if(child.getKeySubstr().charAt(0) == key.charAt(keyIndex)){
				return findNode(child, key, keyIndex+1);
			}
		}

		return null;
	}

	/**
	 * Returns the first value for a given key. null if key is not found.
	 */
	public V find(String key) {
		TrieNode<V> node = findNode(root, key, 0);
		if (node == null) {
			return null;
		}
		return node.getValues().get(0);
	}

	/**
	 * Returns all values for a given key.
	 * 
	 * @return Iterator over all values. If key is not found: Iterator without
	 *         next.
	 */
	public Iterator<V> findAll(String key) {
		TrieNode<V> node = findNode(root, key, 0);
		if (node == null) {
			return new ArrayList<V>().iterator();
		}
		return node.getValues().iterator();
	}

	/**
	 * Removes all values for a given key.
	 */
	public void remove(String key) {
		TrieNode<V> node = findNode(root, key, 0);
		if (node != null) {
			node.getValues().clear();
		}
	}

	/**
	 * Returns the number of values in the trie.
	 */
	public int size() {
		return size(root);
	}

	/**
	 * @return Number of values in this node and its child nodes.
	 */
	private int size(TrieNode<V> element) {
		int size = element.getValues().size();
		for(TrieNode<V> child : element.getChilds()){
			size += size(child);
		}
		return size;
	}

	/**
	 * Print the tree.
	 */
	public void print() {
		print(0, root);
	}

	/**
	 * 
	 * @param depth
	 *            Depth in which the node is.
	 * @param node
	 *            Node to print.
	 */
	private void print(int depth, TrieNode<V> node) {

		// TODO Implement here...

	}

	public static void main(String[] args) {
		TrieMultimap<String> multimap = new TrieMultimap<>();

		multimap.insert("Büro", "bureau");
		multimap.insert("Büro", "office");
		multimap.insert("Büro", "agency");
		multimap.insert("Hallo", "hello");
		multimap.insert("Held", "hero");
		multimap.insert("halten", "keep");
		multimap.insert("Hall", "hall");
		multimap.insert("Halle", "hall");
		multimap.insert("hast", "have");
		multimap.insert("Ekstase", "ecstasy");
		multimap.insert("Ecke", "corner");
		multimap.insert("Ecken", "corners");

		if (multimap.size() != 12) {
			System.err.println("wrong size after insertion");
			System.exit(1);
		}

		System.out.println("after insertion");
		multimap.print();

		System.out.println("\nfind test:");
		System.out.println(multimap.find("Büro"));
		System.out.println(multimap.find("Hallo"));
		System.out.println(multimap.find("Held"));
		System.out.println(multimap.find("halten"));
		System.out.println(multimap.find("Hall"));
		System.out.println(multimap.find("Halle"));
		System.out.println(multimap.find("hast"));
		System.out.println(multimap.find("Ekstase"));
		System.out.println(multimap.find("Ecke"));
		System.out.println(multimap.find("Ecken"));
		System.out.println(multimap.find("XYZ"));

		System.out.println("\nfindall test:");
		Iterator<String> it = multimap.findAll("Büro");
		while (it.hasNext()) {
			System.out.print(it.next() + " ");
		}
		System.out.println();
		System.out.println(multimap.findAll("XYZ").hasNext());

		if (multimap.size() != 12) {
			System.err.println("wrong after find routines");
			System.exit(2);
		}

		multimap.remove("Hallo");
		multimap.remove("halten");
		multimap.remove("Ecke");
		multimap.remove("hast");
		multimap.remove("H");
		System.out.println();

		if (multimap.size() != 8) {
			System.err.println("wrong after remove");
			System.exit(3);
		}
		System.out.println("after remove");
		multimap.print();
	}
}

/*
 * Session-Log:
 * 
 * after insertion : Büro: bureau, office, agency H: eld: hero all: hall o:
 * hello e: hall ha: lten: keep st: have E: kstase: ecstasy cke: corner n:
 * corners
 * 
 * find test: bureau hello hero keep hall hall have ecstasy corner corners null
 * 
 * findall test: bureau office agency false
 * 
 * after remove : Büro: bureau, office, agency H: eld: hero all: hall e: hall E:
 * kstase: ecstasy cken: corners
 * 
 */
