/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Tue Nov 29 10:59:03 CET 2016
 */

package uebung11.as.aufgabe03;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JFrame;

import uebung11.as.aufgabe02.Graph;
import uebung11.as.aufgabe02.GraphView;
import uebung11.as.aufgabe02.Node;

/**
 * @author tbeeler
 * @author msuess
 * @version 0.2
 */
public class GraphViewer {

  private static JFrame frame;

  private static String startURL;

  private static String stopURL;

  public static void setURLs(String start, String stop) {
    startURL = start;
    stopURL = stop;
  }

  // create and show the GUI
  private static void createAndShowGUI() {
    if (frame != null) {
      frame.setVisible(false);
    }

    // make sure we have nice window decorations
    JFrame.setDefaultLookAndFeelDecorated(true);

    // create and set up the window
    frame = new JFrame("Graph Viewer");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Node<String> startNode = new Node<String>(startURL);
    Node<String> stopNode = new Node<String>(stopURL);
    ArrayList<Node<String>> toVisit = new ArrayList<Node<String>>();
    toVisit.add(startNode);
    Graph<String> graph = new Graph<String>();
    ArrayList<Node<String>> path = new ArrayList<Node<String>>();
    Crawler crawler = new Crawler(toVisit, graph, startNode, stopNode, true,
        path);
    Thread crawlerThread = new Thread(crawler);
    crawlerThread.start();

    GraphView<String> mainPanel = new GraphView<String>(graph);

    frame.setContentPane(mainPanel);

    frame.setFocusable(true);

    // display the window
    frame.pack();
    frame.setVisible(true);

    frame.addWindowListener(new WindowAdapter() {

      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    mainPanel.setPath(path);
  }

  /**
   * The main function.
   */
  public static void main(String[] args) {
    if (args.length == 2) {
      setURLs(args[0], args[1]);
      // schedule a job for the event-dispatching thread
      // creating and showing this application's GUI
      javax.swing.SwingUtilities.invokeLater(new Runnable() {

        public void run() {
          try {
            createAndShowGUI();
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
    } else {
      System.out
          .println("Usage: java uebungen11.as.aufgabe03.GraphViewer <startURL> <stopURL>");
    }
  }
}
 
 
 
