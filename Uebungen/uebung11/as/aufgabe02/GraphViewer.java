/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Tue Nov 29 10:58:48 CET 2016
 */

package uebung11.as.aufgabe02;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

/**
 * @author tbeeler
 * @author msuess
 * @version 0.2
 */
public class GraphViewer {

  private static JFrame frame;

  // Create and show the GUI
  private static void createAndShowGUI() {
    if (frame != null) {
      frame.setVisible(false);
    }

    // Make sure we have nice window decorations.
    JFrame.setDefaultLookAndFeelDecorated(true);

    // Create and set up the window.
    frame = new JFrame("Graph Viewer");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    
    
    
    
    Map<Character, Node<Character>> nodes = new HashMap<Character, Node<Character>>();
    Graph<Character> graph = new Graph<Character>();
    for (char c = 'A'; c <= 'F'; c++) {
      Node<Character> n = new Node<Character>(new Character(c));
      nodes.put(c, n);
      graph.addNode(n);
    }

//    // Example-Graph for DFS according to the script:
//    nodes.get('A').connectTo(nodes.get('B'));
//    nodes.get('B').connectTo(nodes.get('A'));
//    nodes.get('A').connectTo(nodes.get('C'));
//    nodes.get('C').connectTo(nodes.get('A'));
//    nodes.get('A').connectTo(nodes.get('D'));
//    nodes.get('D').connectTo(nodes.get('A'));
//    nodes.get('A').connectTo(nodes.get('E'));
//    nodes.get('E').connectTo(nodes.get('A'));
//    nodes.get('B').connectTo(nodes.get('C'));
//    nodes.get('C').connectTo(nodes.get('B'));
//    nodes.get('C').connectTo(nodes.get('D'));
//    nodes.get('D').connectTo(nodes.get('C'));
//    nodes.get('C').connectTo(nodes.get('E'));
//    nodes.get('E').connectTo(nodes.get('C'));

    // Example-Graph for BFS according to the script:
		nodes.get('A').connectTo(nodes.get('B'));
		nodes.get('B').connectTo(nodes.get('A'));
		nodes.get('A').connectTo(nodes.get('C'));
		nodes.get('C').connectTo(nodes.get('A'));
		nodes.get('A').connectTo(nodes.get('D'));
		nodes.get('D').connectTo(nodes.get('A'));
		nodes.get('B').connectTo(nodes.get('C'));
		nodes.get('C').connectTo(nodes.get('B'));
		nodes.get('C').connectTo(nodes.get('D'));
		nodes.get('D').connectTo(nodes.get('C'));
		nodes.get('B').connectTo(nodes.get('E'));
		nodes.get('E').connectTo(nodes.get('B'));
		nodes.get('C').connectTo(nodes.get('E'));
		nodes.get('E').connectTo(nodes.get('C'));
		nodes.get('C').connectTo(nodes.get('F'));
		nodes.get('F').connectTo(nodes.get('C'));
		nodes.get('D').connectTo(nodes.get('F'));
		nodes.get('F').connectTo(nodes.get('D'));

    // Search for path
    ArrayList<Node<Character>> path = 
      graph.depthFirstSearch(nodes.get('A'), nodes.get('E'));
//    ArrayList<Node<Character>> path = 
//      graph.breadthFirstSearch(nodes.get('A'),nodes.get('F'));
    if (path == null) {
      System.out.println("Path from " + nodes.get('A').getObject() + " to "
          + nodes.get('E').getObject() + " not found!");
    } else {
      System.out.println("Path: ");
      for (Node<Character> n : path) {
        System.out.print(n.getObject() + " ");
      }
      System.out.print("\n");
    }
    GraphView<Character> mainPanel = new GraphView<Character>(graph);
    mainPanel.setPath(path);

    
    
    
    
    
    frame.setContentPane(mainPanel);

    frame.setFocusable(true);

    // Display the window.
    frame.pack();
    frame.setVisible(true);

    frame.addWindowListener(new WindowAdapter() {

      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
  }

  /**
   * The main function.
   */
  public static void main(String[] args) {
    // Schedule a job for the event-dispatching thread.
    // Creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable() {

      public void run() {
        try {
          createAndShowGUI();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }
}

 
/* Session-Log:


DFS:
---
A: B 
B: A 
B: C 
C: A 
C: B 
C: D 
D: A 
D: C 
C: E 
Path: 
A B C E 


BFS:
---
ipath: A
it: B  previous: A  current: B
it: C  previous: A  current: C
it: D  previous: A  current: D
ipath: B
it: A
it: C
it: E  previous: B  current: E
ipath: C
it: A
it: B
it: D
it: E
it: F  previous: C  current: F
ipath: D
it: A
it: C
it: F
ipath: E
it: B
it: C
ipath: F
Path: 
A C F 


*/

 
 
