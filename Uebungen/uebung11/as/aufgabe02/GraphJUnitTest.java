/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Tue Nov 29 10:58:48 CET 2016
 */

package uebung11.as.aufgabe02;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GraphJUnitTest {

  Graph<Character> graph;
  Node<Character> nodeA;
  Node<Character> nodeB;
  Node<Character> nodeC;
  Node<Character> nodeD;
  Node<Character> nodeE;

  @Before
  public void setUp() {
    graph = new Graph<Character>();
    nodeA = new Node<Character>('A');
    nodeB = new Node<Character>('B');
    nodeC = new Node<Character>('C');
    nodeD = new Node<Character>('D');
    nodeE = new Node<Character>('E');
    graph.addNode(nodeA);
    graph.addNode(nodeC);
    graph.addNode(nodeB);
    graph.addNode(nodeD);
    graph.addNode(nodeE);
    nodeA.connectTo(nodeC);
    nodeC.connectTo(nodeA);
    nodeA.connectTo(nodeB);
    nodeB.connectTo(nodeA);
    nodeC.connectTo(nodeD);
    nodeD.connectTo(nodeC);
    nodeB.connectTo(nodeD);
    nodeD.connectTo(nodeB);
  }

  @Test
  public void test01DepthFirstSearch1() {
    ArrayList<Node<Character>> path = graph.depthFirstSearch(nodeA, nodeD);
    assertTrue(path.size() == 3);
    assertSame(nodeA, path.get(0));
    assertSame(nodeB, path.get(1));
    assertSame(nodeD, path.get(2));
  }

  @Test
  public void test02DepthFirstSearch2() {
    ArrayList<Node<Character>> path = graph.depthFirstSearch(nodeC, nodeD);
    assertTrue(path.size() == 4);
    assertSame(nodeC, path.get(0));
    assertSame(nodeA, path.get(1));
    assertSame(nodeB, path.get(2));
    assertSame(nodeD, path.get(3));
  }
  
  @Test
  public void test03DepthFirstSearch3() {
    ArrayList<Node<Character>> path = graph.depthFirstSearch(nodeA, nodeE);
    assertNull(path);
  }

  @Test
  public void test04BreadthFirstSearch1() {
    ArrayList<Node<Character>> path = graph.breadthFirstSearch(nodeA, nodeD);
    assertTrue(path.size() == 3);
    assertSame(nodeA, path.get(0));
    assertSame(nodeB, path.get(1));
    assertSame(nodeD, path.get(2));
  }

  @Test
  public void test05BreadthFirstSearch2() {
    ArrayList<Node<Character>> path = graph.breadthFirstSearch(nodeC, nodeD);
    assertTrue(path.size() == 2);
    assertSame(nodeC, path.get(0));
    assertSame(nodeD, path.get(1));
  }
  
  @Test
  public void test06BreadthFirstSearch3() {
    ArrayList<Node<Character>> path = graph.breadthFirstSearch(nodeA, nodeE);
    assertNull(path);
  }

}
 
 
 
