/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Tue Nov 29 10:59:23 CET 2016
 */

package uebung11.ml.aufgabe02;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Stack;


/**
 * @author tbeeler
 */
public class Graph<T extends Comparable<T>> extends Observable {

  /** All nodes in this graph. */
  private ArrayList<Node<T>> nodes;
  private Stack<Node<T>> stack;
  
  /**
   * Create an empty graph.
   */
  public Graph() {
    nodes = new ArrayList<Node<T>>();
    stack = new Stack<Node<T>>();
  }

  /**
   * @return The nodes of the graph.
   */
  public ArrayList<Node<T>> getNodes() {
    return nodes;
  }

  /**
   * @param indx
   *          Index of the node to return.
   * @return The node at index indx.
   */
  public Node<T> getNode(int indx) {
    return nodes.get(indx);
  }

  /**
   * Add a node to this graph.
   * A specific node may only be contained once in the node-list.
   * 
   * @param n
   *          The node to add.
   */
  public void addNode(Node<T> n) {
    if (nodes.contains(n)) {
      return; 
    }
    nodes.add(n);
    setChanged();
    notifyObservers(n);
  }
  
  /**
   * Traditional DFS.
   * 
   * @param from
   *          Start at this node.
   * @param to
   *          Find the path to this node.
   * @return The path if one is found and NULL otherwise.
   */
  public ArrayList<Node<T>> depthFirstSearch(Node<T> from, Node<T> to) {
    from.setMark(true);
    stack.push(from);
    if (from == to) {
      return new ArrayList<Node<T>>(stack);
    }
    for (Node<T> n : from.getConnectedNodes()) {
      System.out.println(from.getObject() + ": " + n.getObject() + " ");
      if (!n.isMarked()) {
        ArrayList<Node<T>> path = depthFirstSearch(n, to);
        if (path != null) {
          return path;
        }
      }
    }
    stack.pop();
    return null;
  }

  /**
   * Traditional BFS expanded by the possibility to extract the path.
   * 
   * @param from
   *          Start at this node.
   * @param to
   *          Find the path to this node.
   * @return The path if one is found and NULL otherwise.
   */
  public ArrayList<Node<T>> breadthFirstSearch(Node<T> from, Node<T> to) {
    ArrayList<IntermediatePath<T>> queue = new ArrayList<>();
    IntermediatePath<T> ip = new IntermediatePath<>(null, from);
    queue.add(ip);
    from.setMark(true);
    while (queue.size() > 0) {
      ip = queue.remove(0);
      System.out.println("ipath: " + ip.current.getObject());
      if (ip.current == to) {
        ArrayList<Node<T>> path = new ArrayList<>();
        do {
          path.add(0, ip.current);
        } while ((ip = ip.previous) != null);
        return path;
      }
      for (Node<T> it : ip.current.getConnectedNodes()) {
        System.out.print("it: " + it.getObject());
        if (!it.isMarked()) {
          it.setMark(true);
          IntermediatePath<T> newIP = new IntermediatePath<T>(ip, it);
          queue.add(newIP);
          System.out.print("  previous: " + newIP.previous.current.getObject()
              + "  current: " + newIP.current.getObject());
        }
        System.out.print('\n');
      }
    }
    return null;
  }

  /**
   * Overwrite observer because setChanged is protected.
   */
  public void notifyObservers() {
    setChanged();
    super.notifyObservers();
  }
}

/**
 * This class is used as the history for the BFS to store the path found so far.
 * Because we don't have a call stack like the DFS has, this extra expense is
 * required. The container is simple and all fields are public. The resulting
 * structure has the form {{...{{{null,start}n1}n2}...}end}.
 * 
 * @author tbeeler
 */
class IntermediatePath<T extends Comparable<T>> {

  /** Link to the previous path. */
  public IntermediatePath<T> previous;

  /** Current node. */
  public Node<T> current;

  public IntermediatePath(IntermediatePath<T> p, Node<T> c) {
    previous = p;
    current = c;
  }
}
 
 
 
