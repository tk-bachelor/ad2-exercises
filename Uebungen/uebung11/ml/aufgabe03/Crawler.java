/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Tue Nov 29 10:59:40 CET 2016
 */

package uebung11.ml.aufgabe03;

import java.util.ArrayList;
import java.util.Optional;

import uebung11.ml.aufgabe02.Graph;
import uebung11.ml.aufgabe02.Node;

/**
 * @author msuess
 */
public class Crawler implements Runnable {

  private ArrayList<Node<String>> toVisit;

  private Graph<String> visited;

  private Node<String> stopNode;

  private Node<String> startNode;

  private HTMLSite html;

  private ArrayList<Node<String>> path;

  private boolean stayInDomain;

  private boolean stop;

  public static final int WAIT_FOR_LINKS = 100; // 5000ms

  public static final int MAX_LINKS = 200;

  public Crawler(ArrayList<Node<String>> toVisit, Graph<String> visited,
      Node<String> startNode, Node<String> stopNode, boolean stayInDomain,
      ArrayList<Node<String>> path) {
    this.path = path;
    this.toVisit = toVisit;
    this.visited = visited;
    this.stopNode = stopNode;
    this.startNode = startNode;
    this.stayInDomain = stayInDomain;
  }

  public void stopCrawling() {
    stop = true;
  }

  /**
   * Add this node to the list of the nodes that need to be visited.
   * Update the connections from anchestor to node.
   * 
   * @param parent
   *          The ancestor of the node.
   * @param n
   *          The node.
   * @return false: If the url of this node is already in the list (toVisit), or 
   *          if it has already been visited.
   */
  private synchronized boolean addToVisit(Node<String> parent, Node<String> n) {
    // make sure the url is not already in toVisit
    String url = n.getObject();
    if (toVisit.stream().anyMatch(node -> node.getObject().equals(url))) {
      return false;
    }
    // check if the url has already been visited, and if so update the parents connections
    Optional<Node<String>> visited = searchInVisited(url);
    if (visited.isPresent()) {
      parent.connectTo(visited.get());
      return false;
    }
    parent.connectTo(n);
    toVisit.add(n);
    return true;
  }

  /**
   * Add this node to the graph with the already visited notes (if not done yet)
   * and remove it from the list of the nodes to be visited.
   * 
   * @param n
   *          The node to be inserted.
   * @return false: If the url has already been visited.
   */
  private synchronized boolean addToVisited(Node<String> n) {
    // make sure, the url is not added multiple times 
    String url = n.getObject();
    if (searchInVisited(url).isPresent()) {
      return false;
    }
    toVisit.remove(n);
    visited.addNode(n);
    return true;
  }
  
  private Optional<Node<String>> searchInVisited(String url) {
    return visited.getNodes().stream()
        .filter(node -> node.getObject().equals(url)).findFirst();
  }

  public void run() {
    int emptyCount = 0;
    String[] newURL;
    String stopURL;
    String linkURL;
    Node<String> link;
    Node<String> n;

    stopURL = stopNode.getObject();

    while (!stop && (visited.getNodes().size() < MAX_LINKS)) {
      try {
        if (!toVisit.isEmpty()) {
          link = toVisit.get(0);
          if (link.getObject() != null) {
            linkURL = link.getObject();
            if (linkURL.equals(stopURL)) {
              stop = true;
            }

            addToVisited(link);

            String domain;
            int end = linkURL.indexOf('/', 7);
            if (end > 6) {
              domain = linkURL.substring(7, end);
            } else {
              domain = linkURL.substring(7);
            }

            Thread.sleep(200);
            html = new HTMLSite(linkURL);
            newURL = html.getOutLinks();

            for (int i = 0; i < newURL.length; i++) {
              if (newURL[i].length() < 3)
                continue;
              if (newURL[i].charAt(newURL[i].length() - 1) == '/')
                newURL[i] = newURL[i].substring(0, newURL[i].length() - 1);
              if ((newURL[i].indexOf('#') == -1)
                  && (newURL[i].indexOf("javascript:") == -1)
                  && (newURL[i].indexOf("exe") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("zip") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("css") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("jpg") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("jpeg") < newURL[i].length() - 4)
                  && (newURL[i].indexOf("mpg") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("mpe") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("wmv") < newURL[i].length() - 3)
                  && (newURL[i].indexOf("mpeg") < newURL[i].length() - 4)
                  && (newURL[i].indexOf("mp3") < newURL[i].length() - 3)) {
                if (newURL[i].indexOf("http://") == 0) {
                  if (stayInDomain) {
                    int newEnd = newURL[i].indexOf('/', 7);
                    if (newEnd > -1) {
                      if (newURL[i].substring(7, newEnd).equals(domain)) {
                        if (newURL[i].equals(stopURL)) {
                          n = stopNode;
                        } else {
                          n = new Node<String>(newURL[i]);
                        }
                        addToVisit(link, n);
                      }
                    } else {
                      if (newURL[i].substring(7).equals(domain)) {
                        if (newURL[i].equals(stopURL)) {
                          n = stopNode;
                        } else {
                          n = new Node<String>(newURL[i]);
                        }
                        addToVisit(link, n);
                      }
                    }
                  } else {
                    if (newURL[i].equals(stopURL)) {
                      n = stopNode;
                    } else {
                      n = new Node<String>(newURL[i]);
                    }

                    addToVisit(link, n);
                  }
                } else if ((newURL[i].indexOf("mailto:") == -1)
                    && (newURL[i].indexOf("https://") == -1)
                    && (newURL[i].indexOf("ftp://") == -1)) {
                  if (newURL[i].charAt(0) == '/') {
                    n = new Node<String>("http://" + domain + newURL[i]);
                  } else {
                    n = new Node<String>("http://" + domain + "/" + newURL[i]);
                  }
                  if ((n.getObject()).equals(stopURL)) {
                    n = stopNode;
                  }

                  addToVisit(link, n);
                } else {
                  // System.out.println("New link found, but
                  // leading somewhere else, not following
                  // ("+newURL[i]+")");
                }
              }
            }
          } else {
            toVisit.remove(link);
          }
          emptyCount = 0;
          Thread.sleep(20);
        } else {
          if (emptyCount++ > WAIT_FOR_LINKS) {
            stop = true;
          }
          Thread.sleep(50);
        }
      } catch (Exception e) {
        System.out.println(e.getMessage());
      }
    }

    ArrayList<Node<String>> partialpath;
    //partialpath = visited.depthFirstSearch(startNode, stopNode);
    partialpath = visited.breadthFirstSearch(startNode,stopNode);
    if (partialpath != null)
      path.addAll(partialpath);
    visited.notifyObservers();
  }
}
 
 
 
