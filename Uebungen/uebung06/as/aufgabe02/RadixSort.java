/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Oct 24 17:07:56 CEST 2016
 */

package uebung06.as.aufgabe02;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * A Radix-Sort which uses internally a Bucket-Sort to sort a list of arrays of
 * strings.
 * 
 * @author mbuehlma
 * 
 */
public class RadixSort {

	// buckets used for bucket sort
	private final LinkedList<String>[] buckets;

	@SuppressWarnings("unchecked")
	RadixSort() {
		// create LinkedList for buckets
		buckets = (LinkedList<String>[]) new LinkedList<?>[1 + ('z' - 'a' + 1)];
		IntStream.range(0, buckets.length).forEach(i -> buckets[i] = new LinkedList<String>());
	}

	public void radixSort(String[] data) {

		// find max index
		AtomicInteger maxLength = new AtomicInteger(-1);
		Arrays.stream(data).forEach(str -> {
			if (str.length() > maxLength.intValue()) {
				maxLength.set(str.length());
			}
		});

		// bucketsort from max index to first index
		for (int i = maxLength.get() - 1; i >= 0; i--) {
			bucketSort(data, i);
		}

	}

	protected void bucketSort(String[] data, int index) {

		// clear buckets
		Arrays.stream(buckets).forEach(list -> list.clear());

		// insert data elements to buckets
		Arrays.stream(data).forEach(str -> {
			if (str.length() <= index) {
				buckets[0].addLast(str);
			} else {
				buckets[str.charAt(index) - 'a' + 1].addLast(str);
			}
		});

		// shift bucket elements back into data array
		AtomicInteger i = new AtomicInteger(0);
		Arrays.stream(buckets).forEach(list -> list.forEach(str -> {
			data[i.getAndIncrement()] = str;
		}));

	}

	public static void main(String[] args) {

		// unsorted data
		final String[] data = new String[] { "bruno", "brach", "auto", "auto", "autonom", "clown", "bismarck", "autark",
				"authentisch", "authentische", "autobahn", "bleibe", "clan" };

		new RadixSort().radixSort(data);

		// verification array, for test purpose only
		final String[] verification;
		// sort the verification array
		verification = data.clone();
		Arrays.sort(verification);

		// print sorted and verify output
		AtomicInteger i = new AtomicInteger(0);
		Arrays.stream(verification).forEach(verificationStr -> {
			if (verificationStr.equals(data[i.get()])) {
				System.out.println(data[i.get()]);
			} else {
				System.err.println("test failed: " + data[i.get()]);
			}
			i.incrementAndGet();
		});

	}

}

/*
 * Session-Log:
 * 
 * autark authentisch authentische auto auto autobahn autonom bismarck bleibe
 * brach bruno clan clown
 * 
 */
