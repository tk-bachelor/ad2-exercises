/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Oct 31 10:15:46 CEST 2016
 */

package uebung06.ml.aufgabe01;

import java.awt.Point;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

/**
 * InPlaceQuickSort from "Data Structures and Algorithms" implemented to use a
 * comparator instead. This allows the usage of multiple keys.
 * 
 * @author tbeeler
 */
public class QuickSort {

  /**
   * @param <T>
   *          Type of elements to be sorted.
   * @param sequence
   *          The sequence to be sorted.
   * @param comp
   *          The comperator to be used.
   * @param a
   *          The leftbound of the part that shall be sorted.
   * @param b
   *          The rightbound of the part that shall be sorted.
   */
  public <T> void inPlaceQuickSort(T[] sequence, Comparator<T> comp, int a, int b) {
    T temp;
    if (a >= b)
      return;
    T pivot = sequence[b];
    int l = a;
    int r = b - 1;
    while (l <= r) {
      while (l <= r && comp.compare(sequence[l], pivot) <= 0) {
        l++;
      }
      while (r >= l && comp.compare(sequence[r], pivot) >= 0) {
        r--;
      }
      if (l < r) {
        temp = sequence[l];
        sequence[l] = sequence[r];
        sequence[r] = temp;
      }
    }
    temp = sequence[l];
    sequence[l] = sequence[b];
    sequence[b] = temp;
    
    // move left and right pointer over the small numbers
    while ((l > a) && (comp.compare(sequence[l], sequence[l-1]) == 0)) { 
      l--;
    }
    while ((r < b) && (r > a) && (comp.compare(sequence[r], sequence[r+1]) == 0)) {
      r++;
    }
    inPlaceQuickSort(sequence, comp, a, l - 1);
    inPlaceQuickSort(sequence, comp, r + 1, b);
  }

  enum SequenceType {RANDOM, EQUAL, SORTED};
  
  public static void main(String[] args) {
    Comparator<Point> comp = new PointComparator();
    QuickSort qs = new QuickSort();
    int nSequence = 50;
    if (args.length > 0)
      nSequence = Integer.parseInt(args[0]);
    final Point s1[] = new Point[nSequence];
    IntStream.range(0, nSequence).forEach(
      i -> s1[i] = new Point((int) (Math.random() * 100),
                             (int) (Math.random() * 100))
    );
    if (nSequence > 300) {
      System.out.println("Too many elements, not printing to stdout.");
    } else {
      Arrays.asList(s1).forEach(point -> 
        System.out.print("(" + point.x + "/" + point.y + "), "));
      System.out.println();
    }
    System.out.print("Quick sort...");
    long then = System.currentTimeMillis();
    qs.inPlaceQuickSort(s1, comp, 0, nSequence - 1);
    long now = System.currentTimeMillis();
    long d1 = now - then;
    System.out.println("done.");
    if (nSequence > 300) {
      System.out.println("Too many elements, not printing to stdout.");
    } else {
      Arrays.asList(s1).forEach(point -> 
        System.out.print("(" + point.x + "/" + point.y + "), "));
      System.out.println();
    }
    System.out.println("Time quick sort [ms]: " + d1);
    
    System.out.println("\nRuntime:");
    final int INITIAL_SIZE = 64; 
    final int NR_OF_DOUBLING = 4;
    Arrays.stream(SequenceType.values()).forEach(seqType -> {
      System.out.println("Sequence-Type: "+seqType);
      final AtomicInteger arraySize = new AtomicInteger(INITIAL_SIZE);
      final AtomicLong lastTime = new AtomicLong(Long.MAX_VALUE);
      IntStream.rangeClosed(0, NR_OF_DOUBLING).forEach(n -> {
        final int MEASUREMENTS = 200;
        AtomicLong minimalTime = new AtomicLong(Long.MAX_VALUE);
        final Point[] s2 = new Point[arraySize.get()];
        IntStream.range(0,  MEASUREMENTS).forEach(m -> {
          IntStream.range(0,  s2.length).forEach(i -> {
            switch (seqType) {
              case RANDOM:
                s2[i] = new Point((int) (Math.random() * 100),
                                  (int) (Math.random() * 100));
                break;
              case EQUAL:
                s2[i] = new Point(1, 1);
                break;
              case SORTED:
                s2[i] = new Point(i, i);
                break;
            }
          });
          long startTime = System.nanoTime();
          qs.inPlaceQuickSort(s2, comp, 0, arraySize.get() - 1);
          long endTime = System.nanoTime();
          long time = endTime - startTime;
          if (time < minimalTime.get()) {
            minimalTime.set(time);
          }
        });
        long minTime = minimalTime.get();
        System.out.format(
          "Array-Size: %,6d   Time: %,7.0f us   Ratio to last: %2.1f\n", 
          arraySize.get(), (double) minTime / (long) 1e3, 
          (double) minTime / lastTime.get());
        lastTime.set(minTime);
        arraySize.set(arraySize.get() * 2);
      });
    });
  }
}

class PointComparator implements Comparator<Point> {

  /**
   * Total order relation for points:
   * p1 > p2 | p1.x > p2.x 
   * p1 > p2 | p1.x = p2.x && p1.y > p2.y
   * p1 = p2 | p1.x = p2.x && p1.y = p2.y 
   * else p1 < p2
   * 
   * @return p1 > p2  : +1, 
   *         p1 == p2 :  0, 
   *         p1 < p2  : -1
   * 
   * @author tbeeler
   */
  public int compare(Point p1, Point p2) {
    if (p1.x < p2.x)
      return -1;
    if (p1.x == p2.x) {
      if (p1.y == p2.y)
        return 0;
      if (p1.y < p2.y)
        return -1;
    }
    return 1;
  }

}

/* Session-Log:

$ java -Xint -Xms10m -Xmx10m uebung06.ml.aufgabe01.QuickSort

(40/5), (66/14), (68/49), (71/14), (93/44), (90/75), (39/80), (86/26), (79/73), (69/87), (7/73), (13/97), (16/83), (63/96), (18/70), (25/85), (95/79), (24/19), (65/2), (78/95), (81/1), (23/15), (45/86), (39/64), (25/96), (26/80), (50/69), (60/85), (16/99), (39/40), (78/98), (97/66), (2/60), (13/31), (39/60), (17/10), (50/32), (89/92), (18/44), (37/81), (5/20), (98/10), (99/86), (74/82), (12/34), (67/3), (68/21), (21/51), (69/11), (99/13), 
Quick sort...done.
(2/60), (5/20), (7/73), (12/34), (13/31), (13/97), (16/83), (16/99), (17/10), (18/44), (18/70), (21/51), (23/15), (24/19), (25/85), (25/96), (26/80), (37/81), (39/40), (39/60), (39/64), (39/80), (40/5), (45/86), (50/32), (50/69), (60/85), (63/96), (65/2), (66/14), (67/3), (68/21), (68/49), (69/11), (69/87), (71/14), (74/82), (78/95), (78/98), (79/73), (81/1), (86/26), (89/92), (90/75), (93/44), (95/79), (97/66), (98/10), (99/13), (99/86), 
Time quick sort [ms]: 0

Runtime:
Sequence-Type: RANDOM
Array-Size:     64   Time:     109 us   Ratio to last: 0.0
Array-Size:    128   Time:     256 us   Ratio to last: 2.3
Array-Size:    256   Time:     569 us   Ratio to last: 2.2
Array-Size:    512   Time:   1,294 us   Ratio to last: 2.3
Array-Size:  1,024   Time:   2,853 us   Ratio to last: 2.2
Sequence-Type: EQUAL
Array-Size:     64   Time:      19 us   Ratio to last: 0.0
Array-Size:    128   Time:      37 us   Ratio to last: 2.0
Array-Size:    256   Time:      74 us   Ratio to last: 2.0
Array-Size:    512   Time:     148 us   Ratio to last: 2.0
Array-Size:  1,024   Time:     296 us   Ratio to last: 2.0
Sequence-Type: SORTED
Array-Size:     64   Time:     307 us   Ratio to last: 0.0
Array-Size:    128   Time:   1,180 us   Ratio to last: 3.8
Array-Size:    256   Time:   4,638 us   Ratio to last: 3.9
Array-Size:    512   Time:  18,467 us   Ratio to last: 4.0
Array-Size:  1,024   Time:  74,368 us   Ratio to last: 4.0

*/

