/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Sat Dec 10 19:28:20 CET 2016
 */

package uebung13.ml.aufgabe02;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ch.hsr.prog2.dijkstragvs.AdjacencyListGraphGVS;
import ch.hsr.prog2.dijkstragvs.GvsSupport;
import net.datastructures.Vertex;

@SuppressWarnings({"unchecked","rawtypes"})

public class DijkstraJUnitTest {

  enum VertexEnum {
    vA("A", 50, 10, 0),
    vB("B", 10, 50, 7),
    vC("C", 50, 50, 2),
    vD("D", 90, 50, 3),
    vE("E", 30, 90, 5),
    vF("F", 70, 90, 8);
    
    public String value;
    public int xPos;
    public int yPos;
    public int distance;
    public Vertex vertex;
    
    VertexEnum(String value, int xPos, int yPos, int distance) {
      this.value = value;
      this.xPos = xPos;
      this.yPos = yPos;
      this.distance = distance;
    }
  }
  
  enum EdgeEnum {
    EdgeAD(VertexEnum.vA.vertex, VertexEnum.vD.vertex, 4),
    EdgeAC(VertexEnum.vA.vertex, VertexEnum.vC.vertex, 2),
    EdgeAB(VertexEnum.vA.vertex, VertexEnum.vB.vertex, 8),
    EdgeCB(VertexEnum.vC.vertex, VertexEnum.vB.vertex, 7),
    EdgeCD(VertexEnum.vC.vertex, VertexEnum.vD.vertex, 1),
    EdgeCE(VertexEnum.vC.vertex, VertexEnum.vE.vertex, 3),
    EdgeCF(VertexEnum.vC.vertex, VertexEnum.vF.vertex, 9),
    EdgeEB(VertexEnum.vE.vertex, VertexEnum.vB.vertex, 2),
    EdgeDF(VertexEnum.vD.vertex, VertexEnum.vF.vertex, 5);
    
    public Vertex v1;
    public Vertex v2;
    public int weight; 
    
    EdgeEnum(Vertex v1, Vertex v2, int weight) {
      this.v1 = v1;
      this.v2 = v2;
      this.weight = weight;
    }
  }
  
  Dijkstra<String, Integer> dijkstra = new Dijkstra<String, Integer>();
  AdjacencyListGraphGVS<String, Integer> graph = (AdjacencyListGraphGVS)dijkstra.getGraph();
  Vertex<String> startVertex;

  @Before
  public void setup() {
  
    GvsSupport.DELAY = 0;
   
    for (VertexEnum v: VertexEnum.values()) {
      v.vertex = graph.insertVertex(v.value, v.xPos, v.yPos);
    }
    
    for (EdgeEnum e: EdgeEnum.values()) {
      graph.insertEdge(e.v1, e.v2, e.weight);
    }

    startVertex = VertexEnum.vA.vertex;
  }
  
  @Test
  public void test() {
    dijkstra.distances(graph, startVertex);
    dijkstra.stop();
    for (VertexEnum v: VertexEnum.values()) {
      Integer distance = (Integer)Dijkstra.gvs.distances.get(v.vertex);
      assertEquals(Integer.valueOf(v.distance), distance);
    }
  }

}
