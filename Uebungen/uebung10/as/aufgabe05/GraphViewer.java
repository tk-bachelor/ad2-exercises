/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 21 17:37:48 CET 2016
 */

package uebung10.as.aufgabe05;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * @author tbeeler
 * @author msuess
 * @version 0.2
 */
public class GraphViewer {

	private static JFrame frame;

	// Create and show the GUI
	private static void createAndShowGUI() {
		if (frame != null) {
			frame.setVisible(false);
		}

		// Make sure we have nice window decorations.
		JFrame.setDefaultLookAndFeelDecorated(true);

		// Create and set up the window.
		frame = new JFrame("Graph Viewer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Graph<Character> graph = new Graph<Character>();

		// Fill up the graph here ...
		final char START_CHAR = 'A';
		final char END_CHAR = 'M';
		for (char c = START_CHAR; c <= END_CHAR; c++) {
			graph.addNode(new Node<Character>(c));
		}
		for (Node<Character> thisNode : graph.getNodes()) {
			for (Node<Character> otherNode : graph.getNodes()) {
				thisNode.connectTo(otherNode);
			}
		}

		GraphView<Character> mainPanel = new GraphView<Character>(graph);

		frame.setContentPane(mainPanel);

		frame.setFocusable(true);

		// Display the window.
		frame.pack();
		frame.setVisible(true);

		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	/**
	 * The main function.
	 */
	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread.
		// Creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				try {
					createAndShowGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
