/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 21 17:37:48 CET 2016
 */

package uebung10.as.aufgabe05;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * @author tbeeler
 */
public class Node<T extends Comparable<T>> {

	/** Object of the node. */
	private T obj;

	/** Connected neighbour nodes. */
	private ArrayList<Node<T>> linked;

	/** Flag to indicate process. */
	private boolean mark;

	/** X-coordinate. */
	private int x;

	/** Y-coordinate. */
	private int y;

	/**
	 * Constructs a node to contain the given object.
	 * 
	 * @param obj
	 *            The object of this node.
	 */
	public Node(T obj) {

		this.obj = obj;
		this.linked = new ArrayList<>();

	}

	public T getObject() {
		return obj;
	}

	public void setObject(T obj) {
		this.obj = obj;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setMark(boolean mark) {
		this.mark = mark;
	}

	public boolean isMarked() {
		return mark;
	}

	public ArrayList<Node<T>> getConnectedNodes() {
		return linked;
	}

	/**
	 * Connect this node to node n. The connection is directed. We store only
	 * one connection (n1,n2), so the connection list is a set. The nodes are
	 * sorted in ascending order.
	 * 
	 * @param n
	 *            The node to connect.
	 */
	public void connectTo(Node<T> n) {

		ListIterator<Node<T>> it = linked.listIterator();
		while (it.hasNext()) {
			Node<T> listNode = it.next();
			int compareResult = n.getObject().compareTo(listNode.getObject());
			if (compareResult == 0) {
				return; // list is a set!
			} else if (compareResult < 0) {
				it.previous();
				it.add(n);
				return;
			}
		}
		// Node has not yet been inserted
		linked.add(n);

	}
}
