/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 21 17:37:48 CET 2016
 */

package uebung10.as.aufgabe05;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * @author tbeeler
 */
public class GraphView<E extends Comparable<E>> extends JPanel implements
    Observer {

  private static final long serialVersionUID = 1L;

  private Graph<E> graph;

  private ArrayList<Node<E>> path;
  private Random rand = new Random(11);

  public GraphView(Graph<E> g) {
    int x, y;
    graph = g;
    graph.addObserver(this);
    Iterator<Node<E>> it = graph.getNodes().iterator();
    Node<E> n;
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    dim.height -= 30;
    dim.width -= 10;
    setPreferredSize(dim);
    while (it.hasNext()) {
      n = it.next();
      x = (int) Math.round(rand.nextDouble() * dim.width);
      y = (int) Math.round(rand.nextDouble() * dim.height);
      n.setX(x);
      n.setY(y);
    }
  }

  /**
   * Paint all nodes and their links
   */
  public void paint(Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    g.clearRect(0, 0, getWidth(), getHeight());
    Iterator<Node<E>> it = graph.getNodes().iterator();
    Iterator<Node<E>> it2;
    Node<E> n, n2;
    int x, y, x0, y0;
    g.setColor(Color.black);
    while (it.hasNext()) {
      n = it.next();
      x = n.getX();
      y = n.getY();
      if (x == 0 && y == 0)
        continue;
      // Draw links
      it2 = n.getConnectedNodes().iterator();
      while (it2.hasNext()) {
        n2 = it2.next();
        x0 = n2.getX();
        y0 = n2.getY();
        if (x0 == 0 && y0 == 0)
          continue;
        g.drawLine(x, y, x0, y0);
      }
    }
    it = graph.getNodes().iterator();
    g.setColor(Color.red);
    while (it.hasNext()) {
      n = it.next();
      x = n.getX();
      y = n.getY();
      if (x == 0 && y == 0)
        continue;
      g.drawOval(x - 2, y - 2, 5, 5);
      g.drawString(n.getObject().toString(), x, y);
    }
    if (path != null && !path.isEmpty()) {
      it = path.iterator();
      g.setColor(Color.blue);
      n2 = it.next();
      x0 = n2.getX();
      y0 = n2.getY();
      g.drawOval(x0 - 2, y0 - 2, 5, 5);
      g.drawString(n2.getObject().toString(), x0, y0);
      while (it.hasNext()) {
        x = x0;
        y = y0;
        n2 = it.next();
        x0 = n2.getX();
        y0 = n2.getY();
        if ((x == 0 && y == 0) || (x0 == 0 && y0 == 0))
          continue;
        g2.setStroke(new BasicStroke(5));
        g2.drawLine(x0, y0, x, y);
        g2.fillOval(x0 - 2, y0 - 2, 7, 7);
        Font f = new Font("Verdana", Font.BOLD, 13);
        FontMetrics fm = g2.getFontMetrics(f);
        g2.setColor(Color.white);
        g2.setStroke(new BasicStroke(1));
        g2.fillRect(x0, y0 - 10, fm.stringWidth(n2.getObject().toString()) + 5,
            18);
        g2.setColor(Color.black);
        g2.drawRect(x0, y0 - 10, fm.stringWidth(n2.getObject().toString()) + 5,
            18);
        g2.setFont(f);
        g2.setColor(Color.blue);
        g2.drawString(n2.getObject().toString(), x0 + 3, y0 + 3);
      }
    }
  }

  /**
   * Something changed. Node added.
   */
  public void update(Observable arg0, Object arg1) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension dim = tk.getScreenSize();
    dim.height -= 60;
    dim.width -= 100;
    Node<E> n = graph.getNode(graph.getNodes().size() - 1);
    int x = (int) Math.round(rand.nextDouble() * dim.width) + 50;
    int y = (int) Math.round(rand.nextDouble() * dim.height) + 30;
    n.setX(x);
    n.setY(y);
    // System.out.println(n.getObject().toString());
    repaint();
  }

  public void setPath(ArrayList<Node<E>> p) {
    path = p;
    repaint();
  }
}
 
 
 
