/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 28 17:29:57 CET 2016
 */

package uebung10.ml.aufgabe05;

import java.util.Observable;
import java.util.ArrayList;

/**
 * @author tbeeler
 */
public class Graph<T extends Comparable<T>> extends Observable {

  /** All nodes in this graph. */
  private ArrayList<Node<T>> nodes;

  /**
   * Create an empty graph.
   */
  public Graph() {
    nodes = new ArrayList<Node<T>>();
  }

  /**
   * @return The nodes of the graph.
   */
  public ArrayList<Node<T>> getNodes() {
    return nodes;
  }

  /**
   * @param indx
   *          Index of the node to return.
   * @return The node at index indx.
   */
  public Node<T> getNode(int indx) {
    return nodes.get(indx);
  }

  /**
   * Add a node to this graph.
   * A specific node may only be contained once in the node-list.
   * 
   * @param n
   *          The node to add.
   */
  public void addNode(Node<T> n) {
    if (nodes.contains(n)) {
      return; // list is a set !
    }
    nodes.add(n);
    setChanged();
    notifyObservers(n);
  }

  /**
   * Overwrite observer because setChanged is protected.
   */
  public void notifyObservers() {
    setChanged();
    super.notifyObservers();
  }
}

 
 
 
