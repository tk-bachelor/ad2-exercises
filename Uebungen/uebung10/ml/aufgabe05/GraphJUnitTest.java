/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 28 17:29:57 CET 2016
 */

package uebung10.ml.aufgabe05;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GraphJUnitTest {

  @Test
  public void test01ConnectedNodesAsSet() {
    Node<Character> thisNode = new Node<>('A');
    Node<Character> otherNode = new Node<>('B');
    thisNode.connectTo(otherNode);
    assertEquals(1, thisNode.getConnectedNodes().size());
    thisNode.connectTo(otherNode);
    assertEquals(1, thisNode.getConnectedNodes().size());
  }
  
  @Test
  public void test02ConnectedNodesSorted() {
    Node<Character> thisNode = new Node<>('X');
    Node<Character> bNode = new Node<>('B');
    thisNode.connectTo(bNode);
    Node<Character> aNode = new Node<>('A');
    thisNode.connectTo(aNode);
    assertSame(aNode, thisNode.getConnectedNodes().get(0));
    assertSame(bNode, thisNode.getConnectedNodes().get(1));
  }
  
  @Test
  public void test03GraphAsSet() {
    Node<Character> aNode = new Node<>('A');
    Graph<Character> graph = new Graph<>();
    graph.addNode(aNode);
    graph.addNode(aNode);
    assertEquals(1, graph.getNodes().size());
  }

}
 
 
 
