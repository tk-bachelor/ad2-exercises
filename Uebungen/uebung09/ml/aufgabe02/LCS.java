/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 21 17:05:48 CET 2016
 */

package uebung09.ml.aufgabe02;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class LCS {
  
  private int tableL[][];
  private String xStr;
  private String yStr;
  
  /**
   * Calculates the L-Table.
   * @param xStr First string (vertical axis of L-Table).
   * @param yStr Second string (horizontal axis of L-Table).
   * @return The calculated L-Table.
   */
  public int[][] calculateTable(final String xStr, final String yStr) {
    this.xStr = xStr;
    this.yStr = yStr;
    int n = xStr.length();  
    int m = yStr.length(); 
    tableL = new int[n+1][m+1];
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
        if (xStr.charAt(i-1) == yStr.charAt(j-1)) {
          tableL[i][j] = tableL[i-1][j-1] + 1;
        }
        else {
          tableL[i][j] = Math.max(tableL[i-1][j], tableL[i] [j-1]);
        }
      }
    }
    return tableL;
  }
  
  /**
   * Prints the L-Table (see 'Session-Log').
   */
  public void printTable() {
    final int CELL_LEN = 4;
    final int X_LEN = tableL.length;
    final int Y_LEN = tableL[0].length;
    System.out.println("LCS-Table:");
    for (int y = 0; y < 2*CELL_LEN; y++) {
      System.out.print(' ');
    }
    for (int y = 0; y < Y_LEN-1; y++) {
      System.out.format("%4s", yStr.charAt(y));
    }
    System.out.println("");
    for (int x = 0; x < X_LEN; x++) {
      if (x == 0) {
        for (int y = 0; y < CELL_LEN; y++) {
          System.out.print(' ');
        }
      }
      else {
        System.out.format("%4s", xStr.charAt(x-1));
      }
      for (int y = 0; y < Y_LEN; y++) {
        System.out.format("%4d", tableL[x][y]);
      }
      System.out.println("");
    }
  }
  
  enum Direction {XY, X, Y, END};
  
  class ActualPosition {
    
    int x;
    int y;
    Direction actualDirection;
    
    ActualPosition(int x, int y) {
      this.x= x;
      this.y= y;
      actualDirection = Direction.XY;
    }
    
    void nextDirection() {
      for (Direction dir: Direction.values()) {
        if (dir.ordinal() == actualDirection.ordinal() + 1) {
          actualDirection = dir;
          break;
        }
      }
    }
    
    int nextX() {
      return x - (actualDirection == Direction.Y ? 0 : 1);
    }
    
    int nextY() {
      return y - (actualDirection == Direction.X ? 0 : 1);
    }
    
  }
  
  /**
   * Searches for all LCS's and returns them in a sorted List.
   * @return The sorted list with all LCS's.
   */
  public List<String> findAll() {
    List<String> lcsList = new LinkedList<>();
    List<Character> charList =  new LinkedList<>();
    Deque<ActualPosition> stack = new LinkedList<>();
    
    // starts at the end of the string
    stack.add(new ActualPosition(xStr.length(), yStr.length()));
    while(!stack.isEmpty()) {
      ActualPosition pos = stack.peek();
      
      // if any of the both strings reached the start position
      if ((pos.x == 0) || (pos.y == 0)) {
        lcsList.add(charList.stream().collect(StringBuilder::new, 
          StringBuilder::append, StringBuilder::append).toString());
        stack.pop();
        continue;
      }
      if (pos.actualDirection == Direction.XY) {
        if (xStr.charAt(pos.x - 1) == yStr.charAt(pos.y - 1)) {
          charList.add(0, xStr.charAt(pos.x - 1));
          stack.push(new ActualPosition(pos.x -1, pos.y -1));
          pos.nextDirection();
          continue;
        } else {
          pos.nextDirection();
        }
      }
      if (pos.actualDirection == Direction.X) {
      if (xStr.charAt(pos.x - 1) == yStr.charAt(pos.y - 1)) { 
        // Backtracking!
        charList.remove(0);
      }
      }
      if (pos.actualDirection == Direction.END) {
        stack.pop();
        continue;
      }
      if (tableL[pos.nextX()][pos.nextY()] == tableL[pos.x][pos.y]) { 
        // X or Y Direction:
        stack.push(new ActualPosition(pos.nextX(), pos.nextY()));
      }
      pos.nextDirection();
    }
    List<String> result = lcsList.stream().sorted().distinct()
      .collect(Collectors.toList());
    return result;
  }

  
  public static void main(String[] args) {
    
    final String Y_TEXT = "CGATAATTGAGA";
    final String X_TEXT = "GTTCCTAATA";
    
    LCS lcs = new LCS();
    
    lcs.calculateTable(X_TEXT, Y_TEXT);
    
    System.out.println("\nText: " + Y_TEXT + " " + X_TEXT + "\n");
    lcs.printTable();
    System.out.print("\n");
    
    List<String> lcsStrings = lcs.findAll();
    System.out.println("Text 1:\t" + Y_TEXT);
    System.out.println("Text 2:\t" + X_TEXT);
    System.out.print("\nLCS's:");
    lcsStrings.stream().forEach(str -> System.out.println("\t" + str));
    
  }
  
}


/* Session-Log:

Text: CGATAATTGAGA GTTCCTAATA

LCS-Table:
           C   G   A   T   A   A   T   T   G   A   G   A
       0   0   0   0   0   0   0   0   0   0   0   0   0
   G   0   0   1   1   1   1   1   1   1   1   1   1   1
   T   0   0   1   1   2   2   2   2   2   2   2   2   2
   T   0   0   1   1   2   2   2   3   3   3   3   3   3
   C   0   1   1   1   2   2   2   3   3   3   3   3   3
   C   0   1   1   1   2   2   2   3   3   3   3   3   3
   T   0   1   1   1   2   2   2   3   4   4   4   4   4
   A   0   1   1   2   2   3   3   3   4   4   5   5   5
   A   0   1   1   2   2   3   4   4   4   4   5   5   6
   T   0   1   1   2   3   3   4   5   5   5   5   5   6
   A   0   1   1   2   3   4   4   5   5   5   6   6   6

Text 1: CGATAATTGAGA
Text 2: GTTCCTAATA

LCS's:  CTAATA
        GTAATA
        GTTTAA

*/
