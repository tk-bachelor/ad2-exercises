package uebung09.ml.aufgabe02;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class LCSRecursion {
	private int tableL[][];
	private String xStr;
	private String yStr;

	/**
	 * Calculates the L-Table.
	 * 
	 * @param xStr
	 *            First string (vertical axis of L-Table).
	 * @param yStr
	 *            Second string (horizontal axis of L-Table).
	 * @return The calculated L-Table.
	 */
	public int[][] calculateTable(final String xStr, final String yStr) {
		this.xStr = xStr;
		this.yStr = yStr;
		int n = xStr.length();
		int m = yStr.length();
		tableL = new int[n + 1][m + 1];
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (xStr.charAt(i - 1) == yStr.charAt(j - 1)) {
					tableL[i][j] = tableL[i - 1][j - 1] + 1;
				} else {
					tableL[i][j] = Math.max(tableL[i - 1][j], tableL[i][j - 1]);
				}
			}
		}
		return tableL;
	}

	/**
	 * Prints the L-Table (see 'Session-Log').
	 */
	public void printTable() {
		final int CELL_LEN = 4;
		final int X_LEN = tableL.length;
		final int Y_LEN = tableL[0].length;
		System.out.println("LCS-Table:");
		for (int y = 0; y < 2 * CELL_LEN; y++) {
			System.out.print(' ');
		}
		for (int y = 0; y < Y_LEN - 1; y++) {
			System.out.format("%4s", yStr.charAt(y));
		}
		System.out.println("");
		for (int x = 0; x < X_LEN; x++) {
			if (x == 0) {
				for (int y = 0; y < CELL_LEN; y++) {
					System.out.print(' ');
				}
			} else {
				System.out.format("%4s", xStr.charAt(x - 1));
			}
			for (int y = 0; y < Y_LEN; y++) {
				System.out.format("%4d", tableL[x][y]);
			}
			System.out.println("");
		}
	}

	/**
	 * Searches for all LCS's and returns them in a sorted List.
	 * 
	 * @return The sorted list with all LCS's.
	 */
	public List<String> findAll() {
		List<String> list = new LinkedList<>();
		Deque<Character> stack = new LinkedList<>();
		find(xStr.length(), yStr.length(), stack, list);
		List<String> result = new LinkedList<>();
		list.stream().sorted().distinct().forEach(str -> result.add(str));
		return result;
	}

	private void find(final int xPos, final int yPos, Deque<Character> stack, List<String> stringList) {
		if ((xPos == 0) || (yPos == 0)) {
			stringList.add(toString(stack));
			return;
		} else {
			if (xStr.charAt(xPos - 1) == yStr.charAt(yPos - 1)) {
				stack.push(xStr.charAt(xPos - 1));
				find(xPos - 1, yPos - 1, stack, stringList);
				stack.pop();
			}
			if (tableL[xPos - 1][yPos] == tableL[xPos][yPos]) {
				find(xPos - 1, yPos, stack, stringList);
			}
			if (tableL[xPos][yPos - 1] == tableL[xPos][yPos]) {
				find(xPos, yPos - 1, stack, stringList);
			}
		}
	}

	private String toString(Deque<Character> stack) {
		StringBuilder str = new StringBuilder();
		stack.stream().forEach(c -> str.append(c));
		return str.toString();
	}

	public static void main(String[] args) {

		final String Y_TEXT = "CGATAATTGAGA";
		final String X_TEXT = "GTTCCTAATA";

		LCS lcs = new LCS();

		lcs.calculateTable(X_TEXT, Y_TEXT);

		System.out.println("\nText: " + Y_TEXT + " " + X_TEXT + "\n");
		lcs.printTable();
		System.out.print("\n");

		List<String> lcsStrings = lcs.findAll();
		System.out.println("Text 1:\t" + Y_TEXT);
		System.out.println("Text 2:\t" + X_TEXT);
		System.out.print("\nLCS's:");
		lcsStrings.stream().forEach(str -> System.out.println("\t" + str));

	}

}
