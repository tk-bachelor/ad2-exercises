/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 14 15:24:47 CET 2016
 */

package uebung09.as.aufgabe02;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class LCS {

	private int tableL[][];
	private String xStr;
	private String yStr;

	/**
	 * Calculates the L-Table.
	 * 
	 * @param xStr
	 *            First string (vertical axis of L-Table).
	 * @param yStr
	 *            Second string (horizontal axis of L-Table).
	 * @return The calculated L-Table.
	 */
	public int[][] calculateTable(final String xStr, final String yStr) {

		tableL = new int[xStr.length() + 1][yStr.length() + 1];
		this.xStr = xStr;
		this.yStr = yStr;

		// row
		for (int r = 1; r < tableL.length; r++) {
			// column
			for (int c = 1; c < tableL[0].length; c++) {
				if (xStr.charAt(r - 1) == yStr.charAt(c - 1)) {
					tableL[r][c] = tableL[r - 1][c - 1] + 1;
				} else {
					tableL[r][c] = Math.max(tableL[r - 1][c], tableL[r][c - 1]);
				}
			}
		}

		return tableL;
	}

	/**
	 * Prints the L-Table (see 'Session-Log').
	 */
	public void printTable() {

		// yStr as horizontal text
		System.out.print(" \t  ");
		for (char c : yStr.toCharArray()) {
			System.out.print(c + " ");
		}
		System.out.println();

		// row
		for (int r = 0; r < tableL.length; r++) {

			// xStr as vertical text
			if (r == 0) {
				System.out.print("\t");
			} else {
				System.out.print(xStr.charAt(r - 1) + "\t");
			}

			// column
			for (int c = 0; c < tableL[0].length; c++) {
				System.out.print(tableL[r][c] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Searches for all LCS's and returns them in a sorted List.
	 * 
	 * @return The sorted list with all LCS's.
	 */
	public List<String> findAll() {
		List<String> lcsList = new LinkedList<>();
		Deque<Character> stack = new LinkedList<>();

		find(xStr.length(), yStr.length(), stack, lcsList);

		List<String> result = new LinkedList<>();
		lcsList.stream().sorted().distinct().forEach(s -> result.add(s));
		return result;
	}

	public void find(final int i, final int m, Deque<Character> stack, List<String> lcs) {
		if (i != 0 && m != 0) {
			// letters at i-1 and m-1 are the same
			if (xStr.charAt(i - 1) == yStr.charAt(m - 1)) {
				stack.push(xStr.charAt(i - 1));
				find(i - 1, m - 1, stack, lcs);
				stack.pop();
			}

			// move along column
			if (tableL[i][m] == tableL[i - 1][m]) {
				find(i-1, m, stack, lcs);
			}
			if (tableL[i][m] == tableL[i][m - 1]) {
				find(i, m-1, stack, lcs);
			}			

		} else {
			lcs.add(toString(stack));
			return;
		}
	}

	private String toString(Deque<Character> stack) {
		StringBuilder str = new StringBuilder();
		stack.stream().forEach(c -> str.append(c));
		return str.toString();
	}

	public static void main(String[] args) {

		final String Y_TEXT = "CGATAATTGAGA";
		final String X_TEXT = "GTTCCTAATA";

		LCS lcs = new LCS();

		lcs.calculateTable(X_TEXT, Y_TEXT);

		System.out.println("\nText: " + Y_TEXT + " " + X_TEXT + "\n");
		lcs.printTable();
		System.out.print("\n");

		List<String> lcsStrings = lcs.findAll();
		System.out.println("Text 1:\t" + Y_TEXT);
		System.out.println("Text 2:\t" + X_TEXT);
		System.out.print("\nLCS's:");
		lcsStrings.stream().forEach(str -> System.out.println("\t" + str));

	}

}

/*
 * Session-Log:
 * 
 * Text: CGATAATTGAGA GTTCCTAATA
 * 
 * LCS-Table: C G A T A A T T G A G A 0 0 0 0 0 0 0 0 0 0 0 0 0 G 0 0 1 1 1 1 1
 * 1 1 1 1 1 1 T 0 0 1 1 2 2 2 2 2 2 2 2 2 T 0 0 1 1 2 2 2 3 3 3 3 3 3 C 0 1 1 1
 * 2 2 2 3 3 3 3 3 3 C 0 1 1 1 2 2 2 3 3 3 3 3 3 T 0 1 1 1 2 2 2 3 4 4 4 4 4 A 0
 * 1 1 2 2 3 3 3 4 4 5 5 5 A 0 1 1 2 2 3 4 4 4 4 5 5 6 T 0 1 1 2 3 3 4 5 5 5 5 5
 * 6 A 0 1 1 2 3 4 4 5 5 5 6 6 6
 * 
 * Text 1: CGATAATTGAGA Text 2: GTTCCTAATA
 * 
 * LCS's: CTAATA GTAATA GTTTAA
 * 
 */
