/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Nov 14 15:24:47 CET 2016
 */

package uebung09.as.aufgabe02;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LCSJUnitTest {

  LCS lcs;

  @Before
  public void setUp() {
    lcs = new LCS();
  }

  @Test
  public void test01() {
    final String Y_TEXT = "CGATAATTGAGA";
    final String X_TEXT = "GTTCCTAATA";
    int[][] tableL = lcs.calculateTable(X_TEXT, Y_TEXT);
    int[][] expectedTable = 
      {
        { 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0 },
        { 0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1 },
        { 0,  0,  1,  1,  2,  2,  2,  2,  2,  2,  2,  2,  2 },
        { 0,  0,  1,  1,  2,  2,  2,  3,  3,  3,  3,  3,  3 },
        { 0,  1,  1,  1,  2,  2,  2,  3,  3,  3,  3,  3,  3 },
        { 0,  1,  1,  1,  2,  2,  2,  3,  3,  3,  3,  3,  3 },
        { 0,  1,  1,  1,  2,  2,  2,  3,  4,  4,  4,  4,  4 },
        { 0,  1,  1,  2,  2,  3,  3,  3,  4,  4,  5,  5,  5 },
        { 0,  1,  1,  2,  2,  3,  4,  4,  4,  4,  5,  5,  6 },
        { 0,  1,  1,  2,  3,  3,  4,  5,  5,  5,  5,  5,  6 },
        { 0,  1,  1,  2,  3,  4,  4,  5,  5,  5,  6,  6,  6 },
      };
    assertArrayEquals(expectedTable, tableL);
    List<String> lcsStrings = lcs.findAll();
    List<String> expectedLcsStrings = Arrays.asList("CTAATA", "GTAATA", "GTTTAA");
    assertTrue(lcsStrings.equals(expectedLcsStrings));
  }
  
  @Test
  public void test02() {
    final String Y_TEXT = "SSJJDSDJ";
    final String X_TEXT = "SJSDJDJJ";
    int[][] tableL = lcs.calculateTable(X_TEXT, Y_TEXT);
    int[][] expectedTable = 
      {
        { 0,  0,  0,  0,  0,  0,  0,  0,  0 },
        { 0,  1,  1,  1,  1,  1,  1,  1,  1 },
        { 0,  1,  1,  2,  2,  2,  2,  2,  2 },
        { 0,  1,  2,  2,  2,  2,  3,  3,  3 },
        { 0,  1,  2,  2,  2,  3,  3,  4,  4 },
        { 0,  1,  2,  3,  3,  3,  3,  4,  5 },
        { 0,  1,  2,  3,  3,  4,  4,  4,  5 },
        { 0,  1,  2,  3,  4,  4,  4,  4,  5 },
        { 0,  1,  2,  3,  4,  4,  4,  4,  5 },
      };
    assertArrayEquals(expectedTable, tableL);
    List<String> lcsStrings = lcs.findAll();
    List<String> expectedLcsStrings = Arrays.asList("SJDDJ", "SJJDJ", "SJSDJ", "SSDDJ", "SSJDJ", "SSJJJ");
    assertTrue(lcsStrings.equals(expectedLcsStrings));
  }

}
 
 
 
