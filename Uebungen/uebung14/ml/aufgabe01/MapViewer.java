/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Dec 19 14:45:43 CET 2016
 */
   
package uebung14.ml.aufgabe01;

import javax.media.opengl.*;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import uebung14.ml.aufgabe01.astarpathfinder.*;

import com.sun.opengl.util.Animator;

/**
 * @author msuess
 */
public class MapViewer extends JFrame {

  private static final long serialVersionUID = 1L;
  private MapView glScene;
  private JPanel infoPanel;
  private JLabel infoLabel;
  private Map map;
  private SearchThread st;
  protected PathFinder pf;

  private double totWeight;

  public MapViewer() {
    this(0);
  }

  public MapViewer(int pathfinder) {
    super("Map Viewer");
    GLCanvas canvas = new GLCanvas();

    totWeight = 0;

    map = new Map(200, 200, 0.001);

    JFrame.setDefaultLookAndFeelDecorated(true);
    getContentPane().setLayout(new BorderLayout());
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
    setSize(1024, 768);
    setLocationRelativeTo(null); // position the frame in the screen center

    glScene = new MapView(getSize().width, getSize().height, map, this);
    canvas.addGLEventListener(glScene);
    canvas.addKeyListener(glScene);
    canvas.addMouseMotionListener(glScene);

    getContentPane().add(canvas, BorderLayout.CENTER);
    infoPanel = new JPanel();
    infoLabel = new JLabel("Total Path Weight: -");
    infoPanel.add(infoLabel);
    getContentPane().add(infoPanel, BorderLayout.NORTH);

    setVisible(true);

    switch (pathfinder) {
      case 1:
        pf = new AStarPathFinder4(map);
        break;
      case 2:
        pf = new AStarPathFinder4Ipol(map);
        break;
      case 3:
        pf = new AStarPathFinder8(map);
        break;
      default:
        pf = new PathFinderImpl(map);
        break;
    }

    searchPath();
    final Animator animator = new Animator(canvas);
    this.addWindowListener(new WindowAdapter() {

      public void windowClosing(WindowEvent e) {
        /* Run this on another thread than the AWT event queue to
         * make sure the call to Animator.stop() completes before
         * exiting.
         */
        new Thread(new Runnable() {

          public void run() {
            animator.stop();
            System.exit(0);
          }
        }).start();
      }
    });
    animator.start();
  }

  public void searchPath() {
    st = new SearchThread(map, (MapView) glScene, this, pf);
    st.start();
  }

  public void setTotWeight(double totWeight) {
    this.totWeight = totWeight;
    updateInfoLabel();
  }

  private void updateInfoLabel() {
    String weight = Math.round(totWeight) == 0 ? "-" : (new Long(Math
        .round(totWeight)).toString());
    infoLabel.setText("Total Path Weight: " + weight);
  }

  public class SearchThread extends Thread implements Observer {

    private Map map;
    private MapView mapView;
    private MapViewer mapViewer;
    private PathFinder pf;

    public SearchThread(Map map, MapView mapView, MapViewer mapViewer,
        PathFinder pf) {
      this.map = map;
      this.mapView = mapView;
      this.mapViewer = mapViewer;
      this.pf = pf;
    }

    public void run() {
      ArrayList<Point> path = new ArrayList<Point>();
      path.add(new Point(0, 0));
      mapView.updatePath(path);
      pf.addObserver(this);
      pf.findPath(2, 2, map.getHeight() - 2, map.getWidth() - 2);
    }

    public void update(Observable arg0, Object arg1) {
      mapView.updatePath(pf.getPath());
      mapViewer.setTotWeight(pf.calcTotalWeight(pf.getPath()));
      pf.deleteObserver(this);
    }
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("No pathfinder selected, starting with default...");
      System.out.println("Usage: MapViewer <pathfinder>");
      System.out.println("\t\t<pathfinder>: 0 = PathFinderImpl");
      System.out.println("\t\t              1 = Four connected A* Search");
      System.out.println("\t\t              2 = Four connected A* Search interpolated");
      System.out.println("\t\t              3 = Eight connected A* Search");
      new MapViewer();
    } else if (args.length == 1) {
      if (args[0].equals("0") || args[0].equals("1") || args[0].equals("2")
          || args[0].equals("3")) {
        new MapViewer(new Integer(args[0]).intValue());
      } else {
        System.out.println("Usage: MapViewer <pathfinder>");
        System.out.println("\t\t<pathfinder>: 0 = PathFinderImpl");
        System.out.println("\t\t              1 = Four connected A* Search");
        System.out.println("\t\t              2 = Four connected A* Search interpolated");
        System.out.println("\t\t              3 = Eight connected A* Search");
      }
    } else {
      System.out.println("Usage: MapViewer <pathfinder>");
      System.out.println("\t\t<pathfinder>: 0 = PathFinderImpl");
      System.out.println("\t\t              1 = Four connected A* Search");
      System.out.println("\t\t              2 = Four connected A* Search interpolated");
      System.out.println("\t\t              3 = Eight connected A* Search");
    }
  }
}
 
 
