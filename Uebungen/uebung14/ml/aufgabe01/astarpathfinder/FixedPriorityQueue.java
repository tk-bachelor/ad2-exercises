/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Mon Dec 19 14:46:06 CET 2016
 */

package uebung14.ml.aufgabe01.astarpathfinder;

import java.util.LinkedList;

/**
 * @author msuess
 * 
 */
public class FixedPriorityQueue {

  private LinkedList<AStarNode> list;
  private int maxCapacity;

  public FixedPriorityQueue(int maxCapacity) {
    this.maxCapacity = maxCapacity;
    list = new LinkedList<AStarNode>();
  }

  public int size() {
    return list.size();
  }

  public boolean isEmpty() {
    return list.isEmpty();
  }

  public AStarNode min() throws EmptyPriorityQueueException {
    if (list.isEmpty()) {
      throw new EmptyPriorityQueueException("The priority queue is empty");
    } else {
      return (AStarNode) list.getFirst();
    }
  }

  public void insert(AStarNode entry) {
    if (list.isEmpty()) {
      list.addFirst(entry);
    } else if (((AStarNode) list.getLast()).weight <= entry.weight) {
      list.addLast(entry);
    } else {
      for (int i = 0; i < list.size(); i++) {
        if (((AStarNode) list.get(i)).weight >= entry.weight) {
          list.add(i, entry);
          break;
        }
      }
    }
    if (list.size() > maxCapacity)
      list.removeLast();
  }

  public AStarNode removeMin() throws EmptyPriorityQueueException {
    if (list.isEmpty()) {
      throw new EmptyPriorityQueueException("The priority queue is empty");
    } else {
      return (AStarNode) list.removeFirst();
    }
  }

  public void print() {
    AStarNode entry;
    System.out.println("Print priority queue entries: ");
    for (int i = 0; i < list.size(); i++) {
      entry = (AStarNode) list.get(i);
      System.out.println(entry.x + "/" + entry.y + ": " + entry.weight);
    }
  }

  public static void main(String[] args) {
    FixedPriorityQueue pq = new FixedPriorityQueue(2);
    pq.insert(new AStarNode(2, 3, 4));
    pq.insert(new AStarNode(5, 4, 6));
    pq.insert(new AStarNode(1, 7, 4));

    pq.print();
  }
}
 
