/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Mon Dec 19 14:46:06 CET 2016
 */

package uebung14.ml.aufgabe01.astarpathfinder;

import java.awt.Point;
import java.util.ArrayList;

import uebung14.ml.aufgabe01.Map;
import uebung14.ml.aufgabe01.PathFinder;
import uebung14.ml.aufgabe01.astarpathfinder.AStarNode;
import uebung14.ml.aufgabe01.astarpathfinder.FixedPriorityQueue;

/**
 * @author msuess
 */
public class AStarPathFinder4 extends PathFinder {

  public static final int MAX_HISTORY = 1000;
  // 4 directed straight
  private static final byte[] xOff = { 0, 1, 0, -1 };
  private static final byte[] yOff = { 1, 0, -1, 0 };
  // 4 directed diagonal
  // private static final byte[] xOff = { 1, 1,-1,-1};
  // private static final byte[] yOff = { 1,-1,-1, 1};

  private FixedPriorityQueue options;
  private byte[][] history;
  private double[][] weights;

  public AStarPathFinder4(Map m) {
    super(m);
    history = new byte[m.getHeight()][m.getWidth()];
    weights = new double[m.getHeight()][m.getWidth()];
    options = new FixedPriorityQueue(MAX_HISTORY);
  }

  public ArrayList<Point> findPath(int x0, int y0, int x1, int y1) {
    int x2, y2;
    double wg;
    AStarNode n;

    path.clear();
    for (int y = 0; y < map.getHeight(); y++) {
      for (int x = 0; x < map.getWidth(); x++) {
        weights[y][x] = Double.MAX_VALUE;
      }
    }

    n = new AStarNode(map.calcDist(x0, y0, x1, y1), x0, y0);
    history[y0][x0] = -1;
    weights[y0][x0] = 0;
    options.insert(n);

    do {
      n = options.removeMin();
      if (n.x == x1 && n.y == y1) {
        // found a path to destination
        int x = x1;
        int y = y1;
        int h;
        do {
          path.add(0, new Point(x, y));
          h = history[y][x];
          if (h >= 0) {
            x -= xOff[h];
            y -= yOff[h];
          }
        } while (h != -1);
        break;
      }

      // path not found yet, thus expanding
      for (byte i = 0; i < xOff.length; i++) {
        x2 = n.x + xOff[i];
        y2 = n.y + yOff[i];
        if (x2 >= 0 && y2 >= 0 && x2 < map.getWidth() && y2 < map.getHeight()) {
          wg = weights[n.y][n.x] + map.calcWeight(n.x, n.y, x2, y2);
          if (wg < weights[y2][x2]) {
            history[y2][x2] = i;
            weights[y2][x2] = wg;
            options.insert(new AStarNode(wg + map.calcDist(x2, y2, x1, y1), x2,
                y2));
          }
        }
      }
    } while (!options.isEmpty());

    setChanged();
    notifyObservers();

    return path;
  }
}
 
