/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Mon Dec 19 14:46:06 CET 2016
 */

package uebung14.ml.aufgabe01.astarpathfinder;

/**
 * @author thabo
 * @author msuess
 */
public class AStarNode {

  public double weight;
  public int x;
  public int y;

  public AStarNode(double weight, int x, int y) {
    this.weight = weight;
    this.x = x;
    this.y = y;
  }
}
 
