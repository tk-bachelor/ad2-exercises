/*
 * HSR - Uebungen Programmieren 2: Algorithmen & Datenstrukturen
 * Version: Mon Dec 19 14:46:06 CET 2016
 */

package uebung14.ml.aufgabe01.astarpathfinder;

/**
 * Runtime exception thrown when one tries to perform operation on an empty
 * priority queue.
 */

public class EmptyPriorityQueueException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EmptyPriorityQueueException(String err) {
    super(err);
  }
}
 
