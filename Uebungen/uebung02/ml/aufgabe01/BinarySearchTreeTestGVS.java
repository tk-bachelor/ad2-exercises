/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Sep 26 11:27:35 CEST 2016
 */

package uebung02.ml.aufgabe01;

public class BinarySearchTreeTestGVS {

  public static void main(String[] args) {

    BinarySearchTreeGVS<Integer, String> bts = new BinarySearchTreeGVS<Integer, String>();

    // example from script: delete internal node
    int[] iarr = { 1, 3, 2, 8, 6, 9, 5 };
    for (int i : iarr) {
      bts.insert(i, "Str" + i);
    }
    bts.remove(bts.find(3));

    bts.gvsTree.disconnect();

  }

}

