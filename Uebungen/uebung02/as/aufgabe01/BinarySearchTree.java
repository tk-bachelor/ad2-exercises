/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Sep 26 11:08:56 CEST 2016
 */

package uebung02.as.aufgabe01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BinarySearchTree<K extends Comparable<? super K>, V> {

	protected Node root;

	public static class Entry<K, V> {

		private K key;
		private V value;

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		protected K setKey(K key) {
			K oldKey = this.key;
			this.key = key;
			return oldKey;
		}

		public K getKey() {
			return key;
		}

		public V setValue(V value) {
			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}

		public V getValue() {
			return value;
		}

		@Override
		public String toString() {
			StringBuilder result = new StringBuilder();
			result.append("[").append(key).append("/").append(value).append("]");
			return result.toString();
		}

	} // End of class Entry

	protected class Node {

		private Entry<K, V> entry;
		private Node leftChild;
		private Node rightChild;

		public Node(Entry<K, V> entry) {
			this.entry = entry;
		}

		public Node(Entry<K, V> entry, Node leftChild, Node rightChild) {
			this.entry = entry;
			this.leftChild = leftChild;
			this.rightChild = rightChild;
		}

		public Entry<K, V> getEntry() {
			return entry;
		}

		public Entry<K, V> setEntry(Entry<K, V> entry) {
			Entry<K, V> oldEntry = entry;
			this.entry = entry;
			return oldEntry;
		}

		public Node getLeftChild() {
			return leftChild;
		}

		public void setLeftChild(Node leftChild) {
			this.leftChild = leftChild;
		}

		public Node getRightChild() {
			return rightChild;
		}

		public void setRightChild(Node rightChild) {
			this.rightChild = rightChild;
		}

	} // End of class Node
	
	private class BooleanHolder{
		boolean isRemoved = false;
	}

	public Entry<K, V> insert(K key, V value) {
		Entry<K, V> entry = new Entry<K, V>(key, value);
		root = insert(root, entry);
		return entry;
	}

	private Node insert(Node node, Entry<K, V> newEntry) {
		if (node == null) {
			return newNode(newEntry);
		} else if (newEntry.getKey().compareTo(node.getEntry().getKey()) <= 0) {
			node.leftChild = insert(node.leftChild, newEntry);
		} else  {
			node.rightChild = insert(node.rightChild, newEntry);
		}
		return node;
	}

	/**
	 * Factory-Method: Creates a new node.
	 * 
	 * @param entry
	 *            The entry to be inserted in the new node.
	 * @return The new created node.
	 */
	protected Node newNode(Entry<K, V> entry) {
		return new Node(entry);
	}

	public void clear() {
		root = null;
	}

	public Entry<K, V> find(K key) {
		return find(root, key);
	}

	private Entry<K, V> find(Node node, K key) {
		if (node == null)
			return null;

		if (key.compareTo(node.entry.key) < 0) {
			return find(node.leftChild, key);
		} else if (key.compareTo(node.entry.key) > 0) {
			return find(node.rightChild, key);
		}

		return node.entry;
	}

	/**
	 * Returns a collection with all entries with key.
	 * 
	 * @param key
	 *            The key to be searched.
	 * @return Collection of all entries found. An empty collection is returned
	 *         if no entry with key is found.
	 */
	public Collection<Entry<K, V>> findAll(K key) {
		Collection<Entry<K, V>> found = new ArrayList<>();
		findAll(found, root, key);		
		return found;
	}

	private void findAll(Collection<Entry<K, V>> found, BinarySearchTree<K, V>.Node node, K key) {
		if (node == null) {
			return;
		}

		int comp = key.compareTo(node.getEntry().getKey());
		if (comp == 0) {
			found.add(node.getEntry());
		}

		// go through child nodes
		if (comp <= 0) {
			findAll(found, node.getLeftChild(), key);
		} 
		
		if (comp >= 0){
			findAll(found, node.getRightChild(), key);
		}
	}

	/**
	 * Returns a collection with all entries in inorder.
	 * 
	 * @return Inorder-Collection of all entries.
	 */
	public Collection<Entry<K, V>> inorder() {
		Collection<Entry<K, V>> found = new ArrayList<>();
		return inorder(found, root);
	}

	private Collection<Entry<K, V>> inorder(Collection<Entry<K, V>> found, Node node) {
		if (node == null) {
			return found;
		}
		inorder(found, node.getLeftChild());
		found.add(node.getEntry());
		inorder(found, node.getRightChild());
		
		return found;
	}

	/**
	 * Prints the entries of the tree as a list in inorder to the console.
	 */
	public void printInorder() {
		Collection<Entry<K,V>> entries = inorder();
		Iterator<Entry<K,V>> it = entries.iterator();
		
		while(it.hasNext()){
			Entry<K,V> next = it.next();
			System.out.print(next.toString());
		}
		System.out.println();
	}

	public Entry<K, V> remove(Entry<K, V> entry) {
		if(entry == null){
			return null;
		}

		BooleanHolder isRemoved = new BooleanHolder();
		root = remove(root, entry, isRemoved);
		
		return isRemoved.isRemoved ? entry : null;
	}

	private Node remove(BinarySearchTree<K, V>.Node node, Entry<K, V> entry, BooleanHolder isRemoved) {
		if(node == null){
			return node;
		}
		
		int comp = entry.getKey().compareTo(node.getEntry().getKey());
		if(comp < 0){
			// go through left child
			node.leftChild = remove(node.leftChild, entry, isRemoved);
		}else if(comp > 0){
			// go through right child
			node.rightChild = remove(node.rightChild, entry, isRemoved);
		}else{
			// key found, but should check the entry since it is a multi-map
			if(node.getEntry() != entry){
				// search for the next entry with this key
				node.leftChild = remove(node.leftChild, entry, isRemoved);
				
				// still not removed
				if(!isRemoved.isRemoved){
					node.rightChild = remove(node.rightChild, entry, isRemoved);
				}
				
				return node;
			}
			
			// key found with correct entry
			if(node.leftChild == null){
				isRemoved.isRemoved = true;
				return node.rightChild;
			}
			
			if(node.rightChild == null){
				isRemoved.isRemoved = true;
				return node.leftChild;
			}
			
			// both children exists
			// find the node with next great value, but less than the right child
			Node q = parentOfNextInorder(node);
			if(node == q){
				node.setEntry(node.rightChild.getEntry());
				node.rightChild = node.rightChild.rightChild;
				isRemoved.isRemoved = true;
			}else{
				node.setEntry(q.leftChild.getEntry());
				q.leftChild = q.leftChild.rightChild;
				
				isRemoved.isRemoved = true;
			}			
		}
		
		return node;
	}

	private BinarySearchTree<K, V>.Node parentOfNextInorder(BinarySearchTree<K, V>.Node node) {
		if(node.rightChild.leftChild != null){
			node = node.rightChild;
			// find min node = go to the left till leaf
			while(node.leftChild.leftChild != null){
				node = node.leftChild;
			}
		}
		return node;
	}

	/**
	 * The height of the tree.
	 * 
	 * @return The actual height. -1 for an empty tree.
	 */
	public int getHeight() {
		return height(root);
	}

	private int height(Node node) {
		if (node == null)
			return -1;

		return Math.max(height(node.leftChild), height(node.rightChild)) + 1;
	}

	public int size() {
		return getSize(root);
	}

	private int getSize(Node node) {
		if (node == null) {
			return 0;
		} else {
			return 1 + getSize(node.getLeftChild()) + getSize(node.getRightChild());
		}
	}

	public boolean isEmpty() {
		return root == null;
	}

	public static void main(String[] args) {

		// Example from lecture "Löschen (IV/IV)":
		BinarySearchTree<Integer, String> bst = new BinarySearchTree<Integer, String>();
		// BinarySearchTree<Integer, String> bst = new
		// BinarySearchTreeGVS<Integer, String>();
		System.out.println("Inserting:");
		bst.insert(1, "Str1");
		bst.printInorder();
		bst.insert(3, "Str3");
		bst.printInorder();
		bst.insert(2, "Str2");
		bst.printInorder();
		bst.insert(8, "Str8");
		bst.printInorder();
		bst.insert(9, "Str9");
		bst.insert(6, "Str6");
		bst.insert(5, "Str5");
		bst.printInorder();

		System.out.println("Removeing 3:");
		Entry<Integer, String> entry = bst.find(3);
		System.out.println(entry);
		bst.remove(entry);
		bst.printInorder();

		// if (bst instanceof BinarySearchTreeGVS) {
		// ((BinarySearchTreeGVS<Integer, String>)bst).gvsTree.disconnect();
		// }

	}

	/*
	 * Session-Log:
	 * 
	 * Inserting: [1/Str1] [1/Str1] [3/Str3] [1/Str1] [2/Str2] [3/Str3] [1/Str1]
	 * [2/Str2] [3/Str3] [8/Str8] [1/Str1] [2/Str2] [3/Str3] [5/Str5] [6/Str6]
	 * [8/Str8] [9/Str9] Removeing 3: [3/Str3] [1/Str1] [2/Str2] [5/Str3]
	 * [6/Str6] [8/Str8] [9/Str9]
	 * 
	 */

} // End of class BinarySearchTree
