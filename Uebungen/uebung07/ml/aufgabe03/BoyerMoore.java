/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Oct 31 15:01:07 CET 2016
 */

package uebung07.ml.aufgabe03;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;


public class BoyerMoore {

  private static int totCount = 0;
  private static int task = 0;

  public static int bmMatch(String t, int startIndex, int[] last, String p) {
    int count = 0;
    int n = t.length();
    int m = p.length();
    int i = startIndex + m - 1;
    int j = m - 1;
    if (i > n - 1)
      return -1;
    do {
      count++;
      if (task == 2) {
        System.out.format("i=%2d  j=%1d    chars i=%c  j=%c\n", i, j, t.charAt(i), p.charAt(j));
      }
      if (t.charAt(i) == p.charAt(j)) {
        if (j == 0) {
          System.out.println("Number of comparison: " + count);
          totCount += count;
          return i;
        } else {
          i--;
          j--;
        }
      } else {
        i = i + m - Math.min(j, 1 + last[t.charAt(i)]);
        j = m - 1;
      }
    } while (i < n);

    System.out.println("Number of comparison: " + count);
    totCount += count;
    return -1;
  }

  static int[] buildLastFunction(String p) {
    int[] last = new int[128];
    for (int i = 0; i < last.length; i++) {
      last[i] = -1;
    }
    for (int i = 0; i < p.length(); i++) {
      last[p.charAt(i)] = i;
    }
    return last;
  }
  
  static void printLastFunction(String t, int[] last) {
    char[] charArr = t.toCharArray();
    Arrays.sort(charArr);
    HashSet<Character> set = new LinkedHashSet<Character>();
    for (int i = 0; i < charArr.length; i++) {
      set.add(charArr[i]);
    }
    System.out.print("last: ");
    for (char c: set) {
      System.out.print(c+"  ");
    }
    System.out.print("\n    ");
    for (char c: set) {
      System.out.format("%3d", last[c]);
    }
    System.out.println('\n');
  }
  
  public static void main(String[] args) {
    String text;
    String pattern;
    pattern = text = "";
    if (args.length == 0 || ((args.length == 1) && args[0].equals("1"))) {
      text = "Anna Kurnikowa war eine Tennisspielerin. Sie spielte wieder ein wenig nachdem ihre Beinverletzung fast wieder geheilt war.";
      pattern = "ein";
      task = 1;
    } else if ((args.length == 1) && args[0].equals("2")) {
      text = "adbaacaabedacedbccede";
      pattern = "daeda";
      task = 2;
    } else {
      if (args.length != 2) {
        System.err.println("Bad number of arguments: " + args.length + " (expected: 2)!");
        System.exit(2);
      }
      text = args[0];
      pattern = args[1];
    }
    System.out.println("Text    : "+text);
    System.out.println("Pattern : "+pattern);
    int res = 0;
    int pos = 0;
    int[] last = buildLastFunction(pattern);
    printLastFunction(text, last);

    while (res >= 0) {
      //System.out.println(text.substring(pos));
      res = bmMatch(text, pos, last, pattern);
      if (res >= 0) {
        System.out.println("Position: " + res);
        pos = res + 1;
        if (pos > text.length() - pattern.length()) {
          break;
        }
      }
    }
    System.out.println();
    System.out.println("Total of comparison: " + totCount);
  }
}
  
/* Session-Log:

$ java uebung12.ml.aufgabe03.BoyerMoore 1
Text    : Anna Kurnikowa war eine Tennisspielerin. Sie spielte wieder ein wenig nachdem ihre Beinverletzung fast wieder geheilt war.
Pattern : ein
last:    .  A  B  K  S  T  a  c  d  e  f  g  h  i  k  l  m  n  o  p  r  s  t  u  v  w  z  
     -1 -1 -1 -1 -1 -1 -1 -1 -1 -1  0 -1 -1 -1  1 -1 -1 -1  2 -1 -1 -1 -1 -1 -1 -1 -1 -1

Number of comparison: 12
Position: 19
Number of comparison: 25
Position: 60
Number of comparison: 13
Position: 84
Number of comparison: 13

Total of comparison: 63


$ java uebung12.ml.aufgabe03.BoyerMoore 2
Text    : adbaacaabedacedbccede
Pattern : daeda
last: a  b  c  d  e  
      4 -1 -1  3  2

i= 4  j=4    chars i=a  j=a
i= 3  j=3    chars i=a  j=d
i= 5  j=4    chars i=c  j=a
i=10  j=4    chars i=d  j=a
i=11  j=4    chars i=a  j=a
i=10  j=3    chars i=d  j=d
i= 9  j=2    chars i=e  j=e
i= 8  j=1    chars i=b  j=a
i=13  j=4    chars i=e  j=a
i=15  j=4    chars i=b  j=a
i=20  j=4    chars i=e  j=a
Number of comparison: 11

Total of comparison: 11

*/
 
 
 
