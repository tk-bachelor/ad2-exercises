/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Oct 31 15:01:07 CET 2016
 */

package uebung07.ml.aufgabe03;


public class KnuthMorrisPratt {

  private static int totCount = 0;
  private static int task = 0;

  public static int kmpMatch(String t, int startIndex, int[] fail, String p) {
    int count = 0;
    int n = t.length();
    int m = p.length();
    int i = startIndex;
    int j = 0;
    while (i < n) {
      count++;
      if (task == 2) {
        System.out.format("i=%2d  j=%1d    chars i=%c  j=%c\n", i, j, t.charAt(i), p.charAt(j));
      }
      if (t.charAt(i) == p.charAt(j)) {
        if (j == m - 1) {
          System.out.println("Number of comparison: " + count);
          totCount += count;
          return i - m + 1;
        } else {
          i++;
          j++;
        }
      } else {
        if (j > 0) {
          j = fail[j - 1];
        } else {
          i++;
        }
      }
    }

    System.out.println("Number of comparison: " + count);
    totCount += count;
    return -1;
  }

  static int[] buildFailureFunction(String p) {
    int[] fail = new int[p.length()];
    fail[0] = 0;
    int i = 1;
    int j = 0;
    while (i < p.length()) {
      if (p.charAt(i) == p.charAt(j)) {
        fail[i] = j + 1;
        i++;
        j++;
      } else if (j > 0) {
        j = fail[j - 1];
      } else {
        fail[i] = 0;
        i++;
      }
    }
    return fail;
  }
  
  static void printFailureFunction(String p, int[] fail) {
    System.out.print("fail: ");
    for (int i = 0; i < p.length(); i++) {
      System.out.print(p.charAt(i)+"  ");
    }
    System.out.print("\n    ");
    for (int i = 0; i < p.length(); i++) {
      System.out.format("%3d", fail[i]);
    }
    System.out.println('\n');
  }
  
  public static void main(String[] args) {
    String text;
    String pattern;
    pattern = text = "";
    if (args.length == 0 || ((args.length == 1) && args[0].equals("1"))) {
      text = "Anna Kurnikowa war eine Tennisspielerin. Sie spielte wieder ein wenig nachdem ihre Beinverletzung fast wieder geheilt war.";
      pattern = "ein";
      task = 1;
    } else if ((args.length == 1) && args[0].equals("2")) {
      text = "dcdadaeddaeadaeddadae";
      pattern = "daeda";
      task = 2;
    } else {
      if (args.length != 2) {
        System.err.println("Bad number of arguments: " + args.length + " (expected: 2)!");
        System.exit(2);
      }
      text = args[0];
      pattern = args[1];
    }
    System.out.println("Text    : "+text);
    System.out.println("Pattern : "+pattern);
    int res = 0;
    int pos = 0;
    int[] fail = buildFailureFunction(pattern);
    printFailureFunction(pattern, fail);

    while (res >= 0) {
      //System.out.println(text.substring(pos));
      res = kmpMatch(text, pos, fail, pattern);
      if (res >= 0) {
        pos = res + 1;
        System.out.println("Position: " + res);
      }
    }
    System.out.println();
    System.out.println("Total of comparison: " + totCount);
  }
}
  
/* Session-Log:

$ java uebung12.ml.aufgabe03.KnuthMorrisPratt 1
Text    : Anna Kurnikowa war eine Tennisspielerin. Sie spielte wieder ein wenig nachdem ihre Beinverletzung fast wieder geheilt war.
Pattern : ein
fail: e  i  n  
      0  0  0

Number of comparison: 22
Position: 19
Number of comparison: 52
Position: 60
Number of comparison: 29
Position: 84
Number of comparison: 43

Total of comparison: 146


$ java uebung12.ml.aufgabe03.KnuthMorrisPratt 2
Text    : dcdadaeddaeadaeddadae
Pattern : daeda
fail: d  a  e  d  a  
      0  0  0  1  2

i= 0  j=0    chars i=d  j=d
i= 1  j=1    chars i=c  j=a
i= 1  j=0    chars i=c  j=d
i= 2  j=0    chars i=d  j=d
i= 3  j=1    chars i=a  j=a
i= 4  j=2    chars i=d  j=e
i= 4  j=0    chars i=d  j=d
i= 5  j=1    chars i=a  j=a
i= 6  j=2    chars i=e  j=e
i= 7  j=3    chars i=d  j=d
i= 8  j=4    chars i=d  j=a
i= 8  j=1    chars i=d  j=a
i= 8  j=0    chars i=d  j=d
i= 9  j=1    chars i=a  j=a
i=10  j=2    chars i=e  j=e
i=11  j=3    chars i=a  j=d
i=11  j=0    chars i=a  j=d
i=12  j=0    chars i=d  j=d
i=13  j=1    chars i=a  j=a
i=14  j=2    chars i=e  j=e
i=15  j=3    chars i=d  j=d
i=16  j=4    chars i=d  j=a
i=16  j=1    chars i=d  j=a
i=16  j=0    chars i=d  j=d
i=17  j=1    chars i=a  j=a
i=18  j=2    chars i=d  j=e
i=18  j=0    chars i=d  j=d
i=19  j=1    chars i=a  j=a
i=20  j=2    chars i=e  j=e
Number of comparison: 29

Total of comparison: 29

*/ 
 
 
