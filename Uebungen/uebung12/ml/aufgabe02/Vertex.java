/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Dec  5 22:00:14 CET 2016
 */

package uebung12.ml.aufgabe02;

import gvs.graph.GVSRelativeVertex;
import gvs.typ.vertex.GVSEllipseVertexTyp;
import gvs.typ.vertex.GVSVertexTyp;

public class Vertex implements Comparable<Vertex>, GVSRelativeVertex {

  private String label;
  private GVSVertexTyp type;
  private double xPos;
  private double yPos;

  /**
   * The vertex-constructor.
   * 
   * @param label
   *          The label to be shown on the vertex. 'null' for an empty string.
   * @param type
   *          Defines the 'design' of the vertex (color, etc.). 'null' for
   *          defaults.
   * @param xPos
   *          The X-position of the vertex (0.0 .. 100.0)
   * @param yPos
   *          The Y-position of the vertex (0.0 .. 100.0)
   */
  public Vertex(String label, GVSVertexTyp type, double xPos, double yPos) {
    this.label = label;
    this.type = type;
    this.xPos = xPos;
    this.yPos = yPos;
  }

  @Override
  public String getGVSVertexLabel() {
    return label;
  }

  @Override
  public GVSVertexTyp getGVSVertexTyp() {
    return type;
  }

  public GVSEllipseVertexTyp setGVSVertexTyp(GVSEllipseVertexTyp type) {
    GVSEllipseVertexTyp old = type;
    this.type = type;
    return old;
  }

  @Override
  public double getX() {
    return xPos;
  }

  @Override
  public double getY() {
    return yPos;
  }

  @Override
  public int compareTo(Vertex other) {
    if (label == other.label) {
      return 0;
    } else if (label == null) {
      return +1;
    } else if (other.label == null) {
      return -1;
    } else {
      return label.compareTo(other.label);
    }
  }

  @Override
  public String toString() {
    return label;
  }

}
