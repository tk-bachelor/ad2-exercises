/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Dec  5 22:00:14 CET 2016
 */

package uebung12.ml.aufgabe02;

public class Aufgabe1 {

  public static void main(String[] args) {

    Vertex vertex01 = new Vertex( "1", null, 50, 10);
    Vertex vertex02 = new Vertex( "2", null, 80, 60);
    Vertex vertex03 = new Vertex( "3", null, 80, 20);
    Vertex vertex04 = new Vertex( "4", null, 40, 90);
    Vertex vertex05 = new Vertex( "5", null, 20, 90);
    Vertex vertex06 = new Vertex( "6", null, 90, 90);
    Vertex vertex07 = new Vertex( "7", null, 70, 90);
    Vertex vertex08 = new Vertex( "8", null, 30, 10);
    Vertex vertex09 = new Vertex( "9", null, 10, 10);
    Vertex vertex10 = new Vertex("10", null, 30, 55);
    
    Edge edge0108 = new Edge(null, null, vertex01, vertex08);
    Edge edge0110 = new Edge(null, null, vertex01, vertex10);
    Edge edge0206 = new Edge(null, null, vertex02, vertex06);
    Edge edge0301 = new Edge(null, null, vertex03, vertex01);
    Edge edge0306 = new Edge(null, null, vertex03, vertex06);
    Edge edge0307 = new Edge(null, null, vertex03, vertex07);
    Edge edge0405 = new Edge(null, null, vertex04, vertex05);
    Edge edge0410 = new Edge(null, null, vertex04, vertex10);
    Edge edge0510 = new Edge(null, null, vertex05, vertex10);
    Edge edge0607 = new Edge(null, null, vertex06, vertex07);
    Edge edge0702 = new Edge(null, null, vertex07, vertex02);
    Edge edge0809 = new Edge(null, null, vertex08, vertex09);
    Edge edge0908 = new Edge(null, null, vertex09, vertex08);
    Edge edge1003 = new Edge(null, null, vertex10, vertex03);
    
    Graph graph = new Graph("Aufgabe 1");
    graph.insertVertex(vertex01);
    graph.insertVertex(vertex02);
    graph.insertVertex(vertex03);
    graph.insertVertex(vertex04);
    graph.insertVertex(vertex05);
    graph.insertVertex(vertex06);
    graph.insertVertex(vertex07);
    graph.insertVertex(vertex08);
    graph.insertVertex(vertex09);
    graph.insertVertex(vertex10);
    graph.insertEdge(edge0108);
    graph.insertEdge(edge0110);
    graph.insertEdge(edge0206);
    graph.insertEdge(edge0301);
    graph.insertEdge(edge0306);
    graph.insertEdge(edge0307);
    graph.insertEdge(edge0405);
    graph.insertEdge(edge0410);
    graph.insertEdge(edge0510);
    graph.insertEdge(edge0607);
    graph.insertEdge(edge0702);
    graph.insertEdge(edge0809);
    graph.insertEdge(edge0908);
    graph.insertEdge(edge1003);
    
    graph.displayOnGVS();

    graph.directedDFS();

    graph.disconnectFromGVS();
  }

}

/* Session-Log:
 
depthFirstSearch() : 1
Testing            : 1->10: DISCOVERY
depthFirstSearch() : 10
Testing            : 10->3: DISCOVERY
depthFirstSearch() : 3
Testing            : 3->1: BACK
Testing            : 3->6: DISCOVERY
depthFirstSearch() : 6
Testing            : 6->7: DISCOVERY
depthFirstSearch() : 7
Testing            : 7->2: DISCOVERY
depthFirstSearch() : 2
Testing            : 2->6: BACK
Testing            : 3->7: FORWARD
Testing            : 1->8: DISCOVERY
depthFirstSearch() : 8
Testing            : 8->9: DISCOVERY
depthFirstSearch() : 9
Testing            : 9->8: BACK
depthFirstSearch() : 4
Testing            : 4->10: CROSS
Testing            : 4->5: DISCOVERY
depthFirstSearch() : 5
Testing            : 5->10: CROSS

*/

