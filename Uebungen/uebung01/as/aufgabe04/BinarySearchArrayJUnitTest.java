package uebung01.as.aufgabe04;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BinarySearchArrayJUnitTest {

  BinarySearchArrayTest binarySearchArray = new BinarySearchArrayTest();

  @Before
  public void setUp() {
    binarySearchArray.clear();
  }

  private void test(List<Integer> list) {
    list.stream().forEach(i -> {
      if (binarySearchArray.arrayList.size() == 0) {
        binarySearchArray.arrayList.add(i);
      } else {
        binarySearchArray.add(0, binarySearchArray.arrayList.size() - 1, i);
      }
    });
  }

  @Test
  public void test1() {
    test(Arrays.asList(1, 2));
    assertTrue(binarySearchArray.verify(2, false));
  }

  @Test
  public void test2() {
    test(Arrays.asList(2, 1));
    assertTrue(binarySearchArray.verify(2, false));
  }

  @Test
  public void test3() {
    test(Arrays.asList(1, 1));
    assertTrue(binarySearchArray.verify(2, false));
  }

  @Test
  public void test4() {
    test(Arrays.asList(1, 2, 3));
    assertTrue(binarySearchArray.verify(3, false));
  }

  @Test
  public void test5() {
    test(Arrays.asList(3, 2, 1));
    assertTrue(binarySearchArray.verify(3, false));
  }

  @Test
  public void test6() {
    test(Arrays.asList(3, 1, 2));
    assertTrue(binarySearchArray.verify(3, false));
  }

}
