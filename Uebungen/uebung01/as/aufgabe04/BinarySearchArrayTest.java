/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Mon Sep 19 12:07:05 CEST 2016
 */

package uebung01.as.aufgabe04;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Consumer;

public class BinarySearchArrayTest {

	protected ArrayList<Integer> arrayList;

	public BinarySearchArrayTest() {
		arrayList = new ArrayList<Integer>();
	}

	public void clear() {
		arrayList = new ArrayList<Integer>();
	}

	public void generateTree(int nodes) {
		new Random().ints(nodes, 0, Integer.MAX_VALUE).forEach(i -> {
			if (arrayList.size() == 0)
				arrayList.add(i);
			else
				add(0, arrayList.size() - 1, i);
		});
	}

	/**
	 * Adds 'content' into the ArrayList by applying a Binary-Search.
	 * 
	 * @param lower
	 *            The lower bound (inclusive) of the range where to insert the
	 *            content.
	 * @param upper
	 *            The upper bound (inclusive) of the range where to insert the
	 *            content.
	 * @param content
	 *            The number to insert into to ArrayList.
	 */
	public void add(int lower, int upper, int content) {
		// insert position found
		if(lower == upper){
			if(content >= arrayList.get(lower)){
				arrayList.add(lower+1, content);
			}else{
				arrayList.add(lower,content);
			}
			return;
		}
		
		// search for the position
		int middle = (lower + upper) / 2;
		if(content > arrayList.get(middle)){
			add(middle+1, upper, content);
		} else {
			add(lower == 0 ? 0 : lower - 1, middle, content);
		}
	}

	public boolean verify(int size, boolean exiting) {

		int arrayListSize = arrayList.size();
		if (arrayListSize != size) {
			System.err.println("ERROR: bad size: " + arrayListSize);
			if (exiting) {
				System.exit(1);
			} else {
				return false;
			}
		}

		class Verifier implements Consumer<Integer> {
			int index = -1;
			int lhs = Integer.MIN_VALUE;
			boolean failure = false;

			@Override
			public void accept(Integer rhs) {
				index++;
				if (lhs > rhs) {
					System.err.format("ERROR: wrong order at [%d]: %d > %d\n", index, lhs, rhs);
					failure = true;
				}
				lhs = rhs;
			}
		}
		Verifier verifier = new Verifier();
		arrayList.stream().forEach(verifier);
		if (verifier.failure) {
			if (arrayListSize < 20) {
				print();
			}
			if (exiting) {
				System.exit(2);
			} else {
				return false;
			}
		}
		return true;
	}

	public void print() {
		System.out.print("{ ");
		int[] counter = { 0 };
		arrayList.stream().forEach(i -> {
			counter[0] = ++counter[0];
			System.out.print(i);
			if (counter[0] != arrayList.size()) {
				System.out.print(", ");
			}
		});
		System.out.println(" }");
	}

	public static void main(String[] args) {
		System.out.println("ARRAYLIST based TEST");
		System.out.println("Please be patient, the following operations may take some time...");
		final int BEGINSIZE = 10000;
		final int TESTRUNS = 100;
		final int VARYSIZE = 10;

		BinarySearchArrayTest binarySearchArray = new BinarySearchArrayTest();
		double avgTime = 0;
		long startTime;
		for (int i = 0; i < TESTRUNS; i++) {
			binarySearchArray.clear();
			startTime = System.currentTimeMillis();
			int size = BEGINSIZE + i * VARYSIZE;
			binarySearchArray.generateTree(size);
			avgTime = ((avgTime * i) + (System.currentTimeMillis() - startTime)) / (i + 1);
			binarySearchArray.verify(size, true);
			// arrayListTest.print();
		}

		System.out.println("Test successful, result is as follows:");
		System.out.println("Average time for generation is: " + avgTime + " ms");
	}

}

/*
 * Session-Log:
 * 
 * ARRAYLIST based TEST Please be patient, the following operations may take
 * some time... Test successful, result is as follows: Average time for
 * generation is: 5.16ms
 * 
 */
