/*
 * HSR - Uebungen 'Algorithmen & Datenstrukturen 2'
 * Version: Thu Oct 13 11:18:24 CEST 2016
 */

package uebung05.ml.aufgabe02;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Random;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BubbleSortJUnitTest {

  @Test
  public void test01() {
    Integer[] arr = {3, 1, 2};
    sort(arr);
  }
  
  @Test
  public void test02() {
    Integer[] arr = {2, 3, 1};
    sort(arr);
  }
  
  @Test
  public void test03() {
    Integer[] arr = {2, 1};
    sort(arr);
  }
  
  @Test
  public void test04() {
    Integer[] arr = {1, 2};
    sort(arr);
  }
  
  @Test
  public void test05() {
    Integer[] arr = {1};
    sort(arr);
  }
  
  @Test
  public void test06() {
    Integer[] arr = {};
    sort(arr);
  }

  @Test
  public void test07StressTest() {
    final int NUMBER_OF_TESTS = 1000;
    final int LENGTH = 100;
    Integer[] arr;
    for (int n = 0; n < NUMBER_OF_TESTS; n++) {
      arr = new Integer[LENGTH];
      class Index{int i;}
      Index index = new Index();
      final Integer[] iarr = arr;
      new Random().ints(LENGTH, 0, 11).forEach(nr -> {
        iarr[index.i++] = nr;
      });
      sort(arr);
    }
  }
  
  private void sort(Integer[] arr) {
    Integer[] clonedArr = arr.clone();
    BubbleSort.bubbleSort1(arr);
    verify(clonedArr, arr);
    arr = clonedArr.clone();
    BubbleSort.bubbleSort2(arr);
    verify(clonedArr, arr);
  }
  
  private void verify(Integer[] orgArr, Integer[] sortedArr) {
    Integer[] sortedOrgArr = new Integer[orgArr.length];
    System.arraycopy(orgArr, 0, sortedOrgArr, 0, orgArr.length);
    Arrays.sort(sortedOrgArr);
    assertArrayEquals(sortedOrgArr, sortedArr);
  }
  
}
 
