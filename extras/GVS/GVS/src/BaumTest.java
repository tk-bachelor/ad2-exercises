

import gvs.tree.GVSTreeWithRoot;

/**
 * @author jjoller
 *  
 */
public class BaumTest {
	public static void main(String args[]) throws InterruptedException {
                                                                        GVSTreeWithRoot gvsTree = null;
    Baum B = null;

    System.out.println("           5         ");
    System.out.println("      3         9    ");
    System.out.println("    2   4     8   10 ");
    System.out.println(" 1           7       ");
    System.out.println("           6         ");
    B = new Baum();                                                     gvsTree= new GVSTreeWithRoot("Binary-Search-Tree GVS");
    B.einfuegen(5);                                                     gvsTree.setRoot(B.getWurzel()); gvsTree.display();
    B.einfuegen(3);                                                     gvsTree.display();
    B.einfuegen(2);                                                     gvsTree.display();
    B.einfuegen(1);                                                     gvsTree.display();
    B.einfuegen(4);                                                     gvsTree.display();
    B.einfuegen(9);                                                     gvsTree.display();
    B.einfuegen(8);                                                     gvsTree.display();
    B.einfuegen(7);                                                     gvsTree.display();
    B.einfuegen(6);                                                     gvsTree.display();
    B.einfuegen(10);                                                    gvsTree.display();
    System.out.println(B);                                              
    B.entfernen(5);                                                     gvsTree.setRoot(B.getWurzel()); gvsTree.display();
                                                                        gvsTree.disconnect();                                    
                                                                        Thread.sleep(2000);
    System.out.println(B);
    
    /* Session-Log: 

    Preorder : (5)(3)(2)(1)(4)(9)(8)(7)(6)(10)
    Inorder  : (1)(2)(3)(4)(5)(6)(7)(8)(9)(10)
    Postorder: (1)(2)(4)(3)(6)(7)(8)(10)(9)(5)
    Preorder : (6)(3)(2)(1)(4)(9)(8)(7)(10)
    Inorder  : (1)(2)(3)(4)(6)(7)(8)(9)(10)
    Postorder: (1)(2)(4)(3)(7)(8)(10)(9)(6)
    
    */

    
    System.out.println("\nvatersymnach: q == p :");
    System.out.println("           5          ");
    System.out.println("      3       9       ");
    System.out.println("    2   4       10    ");
    System.out.println(" 1                11  ");
    B = new Baum();                                                     gvsTree= new GVSTreeWithRoot("Binary-Search-Tree GVS: q == p");  
    B.einfuegen(5);                                                     gvsTree.setRoot(B.getWurzel()); gvsTree.display();       
    B.einfuegen(3);                                                     gvsTree.display();                                       
    B.einfuegen(2);                                                     gvsTree.display();                                       
    B.einfuegen(1);                                                     gvsTree.display();                                       
    B.einfuegen(4);                                                     gvsTree.display();                                       
    B.einfuegen(9);                                                     gvsTree.display();                                       
    B.einfuegen(10);                                                    gvsTree.display();                                       
    B.einfuegen(11);                                                    gvsTree.display();                                       
    System.out.println(B);                                              
    B.entfernen(5);                                                     gvsTree.setRoot(B.getWurzel()); gvsTree.display();
    System.out.println(B);                                              gvsTree.disconnect();                             
                                                                        Thread.sleep(2000);
    /* Session-Log:                                                            
    
    vatersymnach: q == p
    Preorder : (5)(3)(2)(1)(4)(9)(10)(11)
    Inorder  : (1)(2)(3)(4)(5)(9)(10)(11)
    Postorder: (1)(2)(4)(3)(11)(10)(9)(5)
    Preorder : (9)(3)(2)(1)(4)(10)(11)
    Inorder  : (1)(2)(3)(4)(9)(10)(11)
    Postorder: (1)(2)(4)(3)(11)(10)(9)
    
    */

    
    System.out.println("\nBeispiel Folien-Skript:");
    System.out.println("        17      ");
    System.out.println("    11      22  ");
    System.out.println("  7   14        "); 
    System.out.println("     12         ");
    B = new Baum();                                                     gvsTree= new GVSTreeWithRoot("BaumTest");
    B.einfuegen(17);                                                    gvsTree.setRoot(B.getWurzel()); gvsTree.display();  
    B.einfuegen(11);                                                    gvsTree.display();                                  
    B.einfuegen(7);                                                     gvsTree.display();                                  
    B.einfuegen(14);                                                    gvsTree.display();                                  
    B.einfuegen(12);                                                    gvsTree.display();                                  
    B.einfuegen(22);                                                    gvsTree.display();                                  
    System.out.println(B);                                              
    
    
    System.out.println(B.suchen(14));
    System.out.println(B.suchen(8));
    B.entfernen(17);                                                    gvsTree.setRoot(B.getWurzel()); gvsTree.display();      
    B.entfernen(7);                                                     gvsTree.display();      
    B.entfernen(12);                                                    gvsTree.display();      
    B.entfernen(14);                                                    gvsTree.display();      
    B.entfernen(22);                                                    gvsTree.setRoot(B.getWurzel()); gvsTree.display();      
    B.entfernen(11);                                                    gvsTree.setRoot(B.getWurzel()); gvsTree.display();
    System.out.println(B);                                              gvsTree.disconnect(); 
    
    
    /* Session-Log:

    Preorder : (17)(11)(7)(14)(12)(22)
    Inorder  : (7)(11)(12)(14)(17)(22)
    Postorder: (7)(12)(14)(11)(22)(17)
    14
    null
    Preorder : 
    Inorder  : 
    Postorder: 

    */
 
    return ;

  }
}

