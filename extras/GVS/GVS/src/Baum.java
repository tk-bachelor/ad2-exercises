


/**
 * @author jjoller
 *  
 */

public class Baum {
	Knoten Wurzel;

	Baum() {
		Wurzel = null;
    
	}

	public Knoten suchen(int k) {
		return suchen(Wurzel, k);
	}

	Knoten suchen(Knoten p, int x) {
		if (p == null)
			return null;
		if (x < p.key)
			return suchen(p.leftson, x);
		else if (x > p.key)
			return suchen(p.rightson, x);
		return p;
	}

  public void einfuegen(int k) {
    Wurzel = einfuegen(Wurzel, k);
  }

	Knoten einfuegen(Knoten p, int k) {
		if (p == null)
			return new Knoten(k);
		else if (k < p.key)
			p.leftson = einfuegen(p.leftson, k);
		else if (k > p.key)
			p.rightson = einfuegen(p.rightson, k);
		return p;
	}

	Knoten vatersymnach(Knoten p) {
		if (p.rightson.leftson != null) {
			p = p.rightson;
			while (p.leftson.leftson != null)
				p = p.leftson;
		}
		return p;
	}

  public void entfernen(int k) {
    Wurzel = entfernen(Wurzel, k);
  }

	Knoten entfernen(Knoten p, int k) {
		if (p == null)
			return p;
		if (k < p.key)
			p.leftson = entfernen(p.leftson, k);
		else if (k > p.key)
			p.rightson = entfernen(p.rightson, k);
		else {
			if (p.leftson == null)
				return p.rightson;
			if (p.rightson == null)
				return p.leftson;
			Knoten q = vatersymnach(p);
			if (q == p) {
				p.key = p.rightson.key;
				q.rightson = q.rightson.rightson;
			} else {
				p.key = q.leftson.key;
				q.leftson = q.leftson.rightson;
			}
		}
		return p;
	}

	String Inorder(Knoten p) {
		if (p == null)
			return new String("");
		StringBuffer st = new StringBuffer(Inorder(p.leftson) + "("
				+ p.toString() + ")" + Inorder(p.rightson));
		return st.toString();
	}

	String Preorder(Knoten p) {
		if (p == null)
			return new String("");
		StringBuffer st = new StringBuffer("(" + p.toString() + ")"
				+ Preorder(p.leftson) + Preorder(p.rightson));
		return st.toString();
	}

	String Postorder(Knoten p) {
		if (p == null)
			return new String("");
		StringBuffer st = new StringBuffer(Postorder(p.leftson)
				+ Postorder(p.rightson) + "(" + p.toString() + ")");
		return st.toString();
	}

	public String toString() {
		StringBuffer st = new StringBuffer("Preorder : " + Preorder(Wurzel)
				+ "\n" + "Inorder  : " + Inorder(Wurzel) + "\n" + "Postorder: "
				+ Postorder(Wurzel));
		return st.toString();
	}
  
  
  
  Knoten getWurzel() {
    return Wurzel;
  }

}

