

import java.util.Vector;

import gvs.tree.GVSTreeWithRoot;

public class ClusterSplitterTest {

  public static void main(String[] args) {
   
    GVSTreeWithRoot tree = new GVSTreeWithRoot("ClusterSplitter-Test");
    
    final int HEIGHT = 7;
   
    int nodes = (int)Math.pow(2, HEIGHT+1) - 1;
    Vector<MyBinaryNode> list = new Vector<MyBinaryNode>();
    list.setSize(nodes+1);
    for (int i = 1; i <= nodes; i++) {
      list.set(i, new MyBinaryNode(""+i, null, null, null));
    }
    int i = nodes;
    while (i > 1) {
      list.get(i/2).rigth = list.get(i);
      list.get(i/2).left = list.get(i-1);
      i -= 2;
    }
    
    // Works:
    list.get(1).rigth = null;
    list.get(2).left = null;
    for (int j = 85; j <= 89; j++) {
      list.get(j).left = null;
      list.get(j).rigth = null;
    }

//    // No Solution:
//    list.get(7).rigth = null;
    
    tree.setRoot(list.get(1));
    tree.display();
    tree.disconnect();
    
  }

}
