

import gvs.tree.GVSBinaryTreeNode;
import gvs.typ.node.GVSNodeTyp;

/**
 * @author jjoller
 *  
 */

public class Knoten implements GVSBinaryTreeNode {
	Knoten leftson, rightson;
	int key;

	Knoten(int key) {
		this.key = key;
	}

	public String toString() {
		StringBuffer st = new StringBuffer("" + key);
		return st.toString();
	}

  
  
  
  
  
  
  public GVSBinaryTreeNode getGVSLeftChild() {
    return leftson;
  }

  public GVSBinaryTreeNode getGVSRightChild() {
     return rightson;
  }

  public String getNodeLabel() {
    return new Integer(key).toString();
  }

  public GVSNodeTyp getNodeTyp() {
    return null;
  }
  
}



