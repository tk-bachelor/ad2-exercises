# AD2_Exercises
Algorithm and Data Structures Exercises 2

### Week 1 (22. Sep 2016) - Binary Search Tree with Array
- Binary Search Tree
- BST with ArrayList

### Week 2 (29. Sep 2016) - Multimap BST with Node
- BST implementation (find, insert and remove with node)

### Week 3 (06. Oct 2016) - AVL-Tree
- AVL-Tree Insertion & Rotation (Theory)
- AVL Implementation

### Week 4 (13. Oct 2016) - AVL-Tree rebalance
- AVL-Tree rebalance implementation (rotation)

### Week 5 (20. Oct 2016) - Quick Sort and Bubble Sort
- Quick-sort analysis --> O (n log n)
- Bubble-sort implementation --> O(n^2)

### Week 6 (27. Oct 2016) - Quick Sort and Radix Sort
- Quick-sort implementation --> O (n log n)
- Radix-sort

### Week 7 (03. Nov 2016) - Pattern Matching
- Pattern matching (Boyer-Moore vs Knuth-Morris-Pratt)
- BM & KMP implementation

### Week 8 (10.Nov 2016) - Tries
- Tries Theory (Standard and compressed tries)
- Standard Tries implementation

### Week 9 (17.Nov 2016) - Dynamic Programming
- Knapsack
- Longest Common Subsequence

### Week 10 (24.Nov 2016) - Graph Theory
- Graph Theory (Vertices & Edges)
- Graph ADT

### Week 11 (01.Dec 2016) - Graph Traversal
- Depth-First-Search (DFS) vs Breadth First Search (BFS) Theory 
- DFS and BFS implementation
- Path search in WWW

### Week 12 (08.Dec 2016) - Directed Graph
- Directed DFS

### Week 13 (15.Dec 2016) - Shortest Path
- Dijkstra Algorithm

### Week 14 (22.Dec 2016) - Path Finder
- Dijkstra based PathFinder